import React from 'react';
import { Text, TouchableOpacity, Image, View, ImageBackground, Dimensions } from 'react-native';
import { createStackNavigator, createMaterialTopTabNavigator, createDrawerNavigator } from 'react-navigation';
import ChangePassWord1 from './screens/ChangePassWord1';
import ChooseEvoucher from './screens/TabBarScreen/EvoucherInApp';
import MapScreen from './screens/TabBarScreen/MapScreen'
import BillNoScreen from './screens/BillNoScreen';
import ReadyToCollect from './screens/ReadyToCollect';
import Help from './screens/SideMenuScreen/Help';
import PromotionScreen from './screens/SideMenuScreen/PromotionsScreen';
import TrickNdTreat from './screens/SideMenuScreen/TrickNdTreat';
import InboxScreen from './screens/SideMenuScreen/InboxScreen';
import MyAccountScreen from './screens/MyAccountScreen';
import ActivationScreen from './screens/ActivationScreen'
import BirthdayVoucher from './screens/BirthdayVoucher';
import WelcomeGiftVoucher from './screens/TabBarScreen/WelcomeGiftVoucher';
import EvoucherInStore from './screens/TabBarScreen/EvoucherInStore';
import CheckoutScreen from './screens/TabBarScreen/CheckoutScreen';
import DetailPromotion from './screens/TabBarScreen/DetailPromotion';
import MakeUp from './screens/MakeUp';
import CheckoutPaymentScreen from './screens/TabBarScreen/CheckoutPaymentScreen'
import CheckoutPaymentSuccess from './screens/TabBarScreen/CheckoutPaymentSuccess'
import CheckoutPaymentFailure from './screens/TabBarScreen/CheckoutPaymentFailure'
import CheckoutWebview from './screens/TabBarScreen/CheckoutWebview'
import BackArrow from './components/BackArrow';
import ProductDetailsScreen from './screens/TabBarScreen/ProductDetailsScreen';
import LikedProducts from './screens/TabBarScreen/LikedProducts';
import ItemsPictures from './screens/TabBarScreen/ItemsPictures';
import SelectedCatogiries from './screens/TabBarScreen/SelectedCatogiries';
import MyReviewsScreen from './screens/MyReviewsScreen';
import AddReview from './screens/TabBarScreen/AddReview';
import ProductList from './screens/TabBarScreen/ProductList'
import Icon from 'react-native-vector-icons/FontAwesome';
import Slider from './components/slider';
import RewardsScreen from './screens/SideMenuScreen/RewardsScreen';
import NavigationDrawer from './screens/SideMenuScreen/NavigationDrawer';
import ExchangeAndReturn from './screens/SideMenuScreen/ExchangeAndReturn'
import Privacy from './screens/SideMenuScreen/Privacy'
import Terms from './screens/SideMenuScreen/Terms'
import CompanyProfile from './screens/SideMenuScreen/CompanyProfile'
import Membership from './screens/SideMenuScreen/Membership'
import MyTransaction from './screens/MyTransaction';
import StorePurchase from './screens/StorePurchase';
import StoreLocationListView from './screens/TabBarScreen/StoreLocationListView';

import BottomNavigator from './BottomNavigator'
import { futuraPtMedium } from './styles/styleText'

const Tab2 = createMaterialTopTabNavigator({
    MyTransaction: {
        screen: MyTransaction,
        navigationOptions: (navigation) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Trick and Treat..</Text>,
            tabBarLabel: 'ONLINE PURCHASES',
            headerLeft: null,
        })
    },
    StorePurchase: {
        screen: StorePurchase,
        navigationOptions: (navigation) => ({
            tabBarLabel: 'IN-STORE PURCHASES',
            headerLeft: null,
        })
    },
}, {
        tabBarOptions: {
            style: {
                backgroundColor: '#fff',
                height: 50,
                borderTopColor: 'transparent',
                borderTopWidth: 1,
            },
            labelStyle: { fontSize: 14, fontFamily: futuraPtMedium },
            indicatorStyle: {
                backgroundColor: '#ff4da6', // color of the indicator
                height: 3,
            },
            activeTintColor: 'black',
            inactiveTintColor: 'gray',
            activeBackgroundColor: 'white',
            inactiveBackgroundColor: '#F6F6F6',
            tabBarPosition: "top",
        }
    })

const Tab3 = createMaterialTopTabNavigator({
    StoreLocationListView: {
        screen: StoreLocationListView,
        navigationOptions: (navigation) => ({
            headerTitle: <Text style={{
                flex: 1, color: 'red', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Select Store</Text>,
            tabBarLabel: 'LIST VIEW',
            headerLeft: null,
            tabBarIcon: ({ tintColor }) => (
                <Image style={{ height: 25, width: 25 }} source={require('./assets/home.png')} />
            )
        })
    },
    MapScreen: {
        screen: MapScreen,
        navigationOptions: (navigation) => ({

            tabBarLabel: 'MAP VIEW',
            headerLeft: null,
            tabBarIcon: ({ tintColor }) => (
                <Image style={{ height: 25, width: 25 }} source={require('./assets/home.png')} />
            )
        })
    },
}, {
        tabBarOptions: {
            style: {
                backgroundColor: '#fff',
                height: 50,
                borderTopColor: 'transparent',
                borderTopWidth: 1,
                paddingRight: 5,
                paddingLeft: 5,
                borderTopWidth: 1,

            },
            labelStyle: { fontSize: 16, fontFamily: futuraPtMedium },
            indicatorStyle: {
                backgroundColor: '#ff4da6', // color of the indicator
                height: 3,
            },
            activeTintColor: 'black',
            inactiveTintColor: 'gray',
            activeBackgroundColor: 'white',
            inactiveBackgroundColor: '#F6F6F6',
            tabBarPosition: "top",
        }
    })


export default AppStack = createStackNavigator({
    HomeScreen: {
        screen: BottomNavigator,
        navigationOptions: ({ navigation }) => ({
            header: <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'black', height: 60 }}>
                <TouchableOpacity style={{ marginLeft: 10, marginTop: 25 }} onPress={() => navigation.openDrawer()}>

                    <Icon name="navicon" size={25} color="white" />
                </TouchableOpacity>
                <Image
                    style={{
                        width: 80,
                        height: 30,
                        marginTop: 20
                    }}
                    source={require('./assets/sasalogo1.png')}
                />
                <TouchableOpacity onPress={() => navigation.navigate('MyAccountScreen')}>
                    <Image

                        style={{
                            width: 35,
                            height: 35,
                            marginTop: 20,
                            marginRight: 10
                        }}
                        source={require('./assets/sasa_splash_head.png')}
                    />
                    {/* <Icon4 style={{height:30,width:30,marginLeft:10,marginTop:15,marginRight:10}}name="user-circle" size={25}  color="white" /> */}
                </TouchableOpacity>
            </View>
        }),
    },
    MyReviewsScreen: {
        screen: MyReviewsScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Sign Up</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />

        }),
    },
    TrickNdTreat: {
        screen: TrickNdTreat,
    },
    DetailPromotion: {
        screen: DetailPromotion,
    },
    ChangePassWord1: {
        screen: ChangePassWord1,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Change Password</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />,

        }),
    },
    BillNoScreen: {
        screen: BillNoScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Bill No:#0123</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    AddReview: {
        screen: AddReview,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Add Review</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />

        }),
    },
    NavigationDrawer: {
        screen: NavigationDrawer
    },
    MyAccountScreen: {
        screen: MyAccountScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle:
                <Text style={{flex: 1, color: '#fff', fontSize: 22, fontFamily:futuraPtMedium}}>
                    My Account
                </Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />

        }),
    },
    PromotionScreen: {
        screen: PromotionScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Promotions</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    InboxScreen: {
        screen: InboxScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Inbox</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />

        }),
    },
    MapScreen: {
        screen: MapScreen,
        navigationOptions: ({ navigation }) => ({
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    ProductList: {
        screen: ProductList,
        navigationOptions: ({ navigation }) => ({
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />

        }),
    },
    RewardsScreen: {
        screen: RewardsScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Rewards</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />

        }),
    },
    ReadyToCollect: {
        screen: ReadyToCollect,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Bill No:#0123</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />

        }),
    },
    BirthdayVoucher: {
        screen: BirthdayVoucher,
    },
    WelcomeGiftVoucher: {
        screen: WelcomeGiftVoucher,
    },
    MyReviewsScreen: {
        screen: MyReviewsScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>My Reviews</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    OnlinePurchaseTransaction: {
        screen: Tab2,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>My Transaction</Text>,
            title: 'My Transactions',
            headerLeft: null,
            headerStyle: {
                backgroundColor: 'black'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontSize: 16
            },
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),

    },
    ActivationScreen: {
        screen: ActivationScreen,
        navigationOptions: ({ navigation }) => ({
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    ChooseEvoucher: {
        screen: ChooseEvoucher,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Evoucher</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />

        }),
    },
    EvoucherInStore: {
        screen: EvoucherInStore,
        navigationOptions: ({ navigation }) => ({
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    CheckoutScreen: {
        screen: CheckoutScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Checkout</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    SelectStoreScreen: {
        screen: Tab3,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Location</Text>,
            title: 'My Transactions',
            headerLeft: null,
            headerStyle: {
                backgroundColor: 'black'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontSize: 16
            },
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />,
            // headerRight:
            // <Image source={require('./assets/ic_search_24px.png')} />,
        }),
    },
    CheckoutPaymentScreen: {
        screen: CheckoutPaymentScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Payment</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    CheckoutWebview: {
        screen: CheckoutWebview,
    },
    MakeUp: {
        screen: MakeUp,
    },
    CheckoutPaymentSuccess: {
        screen: CheckoutPaymentSuccess,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Payment Success</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    CheckoutPaymentFailure: {
        screen: CheckoutPaymentFailure,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Payment Failed</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    Help: {
        screen: Help,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Help </Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    ExchangeAndReturn: {
        screen: ExchangeAndReturn,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Exchange and Return </Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    Membership: {
        screen: Membership,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Membership</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    Terms: {
        screen: Terms,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Terms & Condition</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    CompanyProfile: {
        screen: CompanyProfile,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>CompanyProfile</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    Privacy: {
        screen: Privacy,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Privacy Policy</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    SelectedCatogiries: {
        screen: SelectedCatogiries,
        navigationOptions: ({ navigation }) => ({
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    ProductDetailsScreen: {
        screen: ProductDetailsScreen,
    },
    Slider: {
        screen: Slider,
        navigationOptions: ({ navigation }) => ({
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />
        }),
    },
    ItemsPictures: {
        screen: ItemsPictures,
        navigationOptions: ({ navigation }) => ({
            headerRight:
                <TouchableOpacity onPress={() => navigation.goBack(null)}  >
                    <Image style={{ height: 20, width: 20, marginRight: 10 }} source={require('./assets/cross.png')} />
                </TouchableOpacity>
        }),
    },
    LikedProducts: {
        screen: LikedProducts,
        navigationOptions: ({ navigation }) => ({
            headerTitle: <Text style={{
                flex: 1, color: '#fff', fontSize: 22, fontFamily:
                    futuraPtMedium
            }}>Liked Products</Text>,
            headerLeft:
                <BackArrow onPress={() => navigation.goBack(null)} />,
            headerRight:
                <TouchableOpacity  >
                    <Image style={{ height: 30, width: 30, marginRight: 10 }} source={require('./assets/mycart.png')} />
                </TouchableOpacity>

        }),
    },
}, {
        initialRouteName: 'HomeScreen',

    });
