import { StyleSheet, Dimensions } from 'react-native';
import { futuraPtMedium } from './styleText'

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const styles = StyleSheet.create({

  container: {
    backgroundColor: 'white',
    height: 250
  },
  productDetailContainer: {
    backgroundColor: '#fff',
    marginLeft: 10,
    marginTop: 10
  },
  productDetailContainerTwo: {
    backgroundColor: '#fff',
    marginLeft: 8
  },
  textHeader: {
    color: '#808080',
    marginTop: 16,
    marginLeft: 10,
    fontSize: 18,
  },

  textHeading: {
    color: '#808080',
    marginTop: 16,
    marginLeft: 2,
    marginRight: 8,
    marginBottom: 10,
    fontSize: 16,
  },
  nameText: {
    color: '#808080',
    marginTop: 1,
    marginLeft: 1,
    fontSize: 14,
  },

  bottomLine: {
    backgroundColor: '#D3D3D3',
    height: 0.50,
    marginRight: 20,
    marginLeft: 20,
    marginTop: 4,

  },

  btnContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 50
  },
  btnTouch: {
    backgroundColor: '#E4007C',
    justifyContent: "center",
    alignItems: 'center',
    height: 50,
    flex: 1, flexDirection: 'row'
  },
  btnText: {
    color: 'white',
    fontSize: 18,
    fontFamily: futuraPtMedium
  },
  styleTextRight: {
    textAlign: 'right', color: '#ff4da6',
  },


  rightContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: '#fff',
    marginBottom: 0





  },
  wrapper: {
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB'
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5'
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9'
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  }



})
export default styles;
