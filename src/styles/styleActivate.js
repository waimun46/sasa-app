import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium, futuraPtBook } from './styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height

const buttonColor='#E4007C'
const textColor='#fff'
const backgroundColor='#fff'

const styles=StyleSheet.create({
   container:{
backgroundColor:'#fff'
   },

    textPhoneNumber:{
        color:'#000',
        marginTop: 16,
        marginBottom:10,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 16,
        justifyContent:"center",
        alignItems:'center',
        fontFamily:futuraPtBook,
        fontWeight:'300'
    },
    inputPhoneContainer: {flex:1, flexDirection:'row', alignItems:'center'},
    inputPhoneText: {color:'#000', marginLeft:10, marginTop: 4, fontFamily:futuraPtBook,fontWeight:'300',fontSize:16},
    inputPhoneNumber:{flex:1, color:'#000', marginLeft: 0, marginRight: 10, marginTop: 4,justifyContent:"center",alignItems:'center',fontFamily:futuraPtBook,fontWeight:'300',fontSize:16},
    inputMembership:{color:'#000', marginLeft: 10, marginRight: 10, marginTop: 4,justifyContent:"center",alignItems:'center',fontFamily:futuraPtBook,fontWeight:'300',fontSize:16},

    lineDown:{borderBottomColor:'#C0C0C0',borderBottomWidth:1,backgroundColor:'#000',height:0.5, marginLeft: 10, marginRight: 10,  marginTop: 4},

    textError:{color:'red',fontSize:15,marginLeft:15,marginTop:10},
    nameError:{
        color: 'red',
        fontSize: 16,
        fontFamily: futuraPtBook,
        fontWeight: '300'
    },
    viewChkMainOne:{flex: 1, flexDirection: 'row'},
    viewChkOne:{marginTop: 16, marginLeft: 10, marginRight: 10},
    chkOne:{backgroundColor: '#f2f2f2', color:'#900', borderRadius: 5},
    viewChkTxtOne:{marginTop: 16, marginLeft: 10, marginRight: 10},
    chkTextOne:{marginRight: 50,fontSize:18,fontWeight:'300',fontFamily:futuraPtBook,color:'#000'},



    viewButton:{marginTop:20,marginLeft:20,marginRight:20,height:50,backgroundColor:buttonColor},
    textButton:{color:textColor,fontSize:20,fontFamily:futuraPtMedium},

  touch:{
      justifyContent:"center",alignItems:'center',height:50
  }



});
export default styles;
