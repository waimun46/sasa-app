import { Platform } from 'react-native'

const OS = Platform.OS;
/*
    grouping fontfamily name for handle different name between android n ios
*/
export const futuraPtMedium = OS == 'ios' ? 'FuturaPT-Medium' : 'futura-pt-medium';
export const futuraPtBook = OS == 'ios' ? 'FuturaPT-Book' : 'futura-pt-book'
export const futuraPtLight = OS == 'ios' ? 'FuturaPT-Light' : 'futura-pt-light'
