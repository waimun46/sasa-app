import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium, futuraPtBook, futuraPtLight } from './styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height
const buttonColor='#ff4da6'
const textColor='#fff'
const backgroundColor='#fff'

const styles=StyleSheet.create({
    container:{
        height:dimHeight
    },
  containerBackground:{
        backgroundColor:backgroundColor
    },
    viewText:{
        marginTop: 6,
        marginBottom: 10,
        marginLeft:10
    },
    textHeading:{
        color:'#000000',
        fontSize:20,
        justifyContent:"center",
        alignItems:'center',
        fontWeight:'300',
        fontFamily:futuraPtMedium
    },
    textNormal:{
        color:'grey',
        fontSize:16,
        marginTop: 10,
        fontWeight:'300',
        justifyContent:"center",
        alignItems:'center',
        fontFamily:futuraPtBook
    },
    textNormal1:{
        color:'grey',
        fontSize:14,
        marginTop: 10,
        fontWeight:'300',
        justifyContent:"center",
        alignItems:'center',
        fontFamily:futuraPtLight
    },
});
export default styles;
