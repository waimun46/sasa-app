import {StyleSheet, Dimensions} from 'react-native';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const styles=StyleSheet.create({

    container:{
     backgroundColor:'#DCDCDC',
     justifyContent:"center",alignItems:'center',
      flex:1
    },
    productDetailContainer:{
     backgroundColor:'#fff',
     marginLeft:8
    },
    productDetailContainerTwo:{
        backgroundColor:'#fff',
        marginLeft:8
    },
    textHeader:{
        color:'#808080', 
        marginTop: 16, 
        marginLeft: 10, 
        fontSize: 18,
    },
    
    textHeadingOne:{
        color:'#000', 
        marginTop: 16, 
        marginLeft: 2, 
        marginRight:8,
        marginBottom:1,
        fontSize: 16,
    },
      textHeadingTwo:{
        color:'#808080', 
        marginTop: 1, 
        marginLeft: 2, 
        marginRight:8,
        marginBottom:10,
        fontSize: 16,
    },
    nameText:{
        color:'#808080', 
        marginTop: 1, 
        marginLeft: 1, 
        fontSize: 14,
    },
 
    bottomLine:{
        backgroundColor:'#D3D3D3',
        height:0.50,
        marginRight:20,
        marginLeft:20,
        marginTop:4,
        
   },
   
    btnContainer:{
    marginTop:10,
    marginBottom:10,
    height:50
  },
   btnTouch:{
    backgroundColor:'#E4007C',
    justifyContent:"center",
    alignItems:'center',
    height:50,
    flex: 1 , flexDirection:'row'
  },
   btnText:{
    color:'white',
    fontSize:18
  },
 styleTextRight:{
       textAlign: 'right', color: '#ff4da6',marginRight:4
   },


   rightContainer: {
    flex: 1,
    marginTop:-20,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    
  },
  rightIcon: {
    resizeMode: 'contain',
   
  },
   viewText:{
        marginTop: 6,
        marginBottom: 10,
        marginLeft:1
    },
     styleViewOne:{
       flex: 1 , flexDirection:'row'
   },
   styleTextRight:{
       textAlign: 'right', color: '#ff4da6',marginRight:4
   },
  

})
export default styles;