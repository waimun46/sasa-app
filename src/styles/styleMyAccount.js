import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium, futuraPtBook } from './styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height
const buttonColor='#ff4da6'
const textColor='#fff'
const backgroundColor='#fff'
const newBackgroundColor='#C0C0C0'

const styles=StyleSheet.create({
    container:{
        height:dimHeight
    },
  containerBackground:{
         backgroundColor:backgroundColor
    },
    blankView:{
        marginTop:4,marginBottom:4,backgroundColor:'#EEEEEE',height:20
    },

     viewText:{
        paddingVertical:10,
        justifyContent:"center",
        alignItems:'center'

    },
    styleTextHi:{
        marginTop: 20,
        // color:'#000',
        fontSize:18,
        fontWeight:'800'
    },
    styleTextName:{
        color:'#000',
        fontSize:18,
    },


    optionViewOne:{
        flex:1 , flexDirection: 'row' , marginTop:10 , marginBottom:10
    },


     viewTextTopOne:{
        marginTop: 6,
        marginBottom: 10,
        marginLeft:15,

    },


    optionView:{
        flex:1 , flexDirection: 'row' , marginTop:1 ,
    },


     viewTextTop:{
        marginTop: 1,


    },
    viewTextEnd:{
         marginTop: 6,
         marginBottom: 10,
         marginLeft:10,

    },
    textHeading:{
         color:'#000000',
        //fontWeight:'bold',
        fontSize:16,
        justifyContent:"center",
        alignItems:'center',
        marginLeft:15,
        fontFamily:futuraPtMedium,
        fontWeight:'300',
    },
    textNormal:{
         color:'#C0C0C0',
        fontWeight:'300',
        fontSize:14,
        marginTop: 2,
        marginBottom: 2,
        marginLeft:15,
       fontFamily:futuraPtBook,
    },
    textName:{
        fontSize: 20,
        fontWeight: '300',
        fontFamily: futuraPtBook,
        color: '#111111',
        marginTop: 20
    },
    textMessage: {
        textAlign:'center',
        fontSize:17,
        fontFamily: futuraPtBook,
        fontWeight: '300'
    },
      lineDown:{
          borderBottomColor:'#C0C0C0',
          borderBottomWidth:1,
          backgroundColor:'#000',
          height:0.5,
          width:dimWidth
            },
        textLogout:{
        color:'#ff4da6',
        fontSize:14,
        justifyContent:"center",
        alignItems:'center',
         marginTop: 10,
        marginBottom: 10,
        fontFamily:futuraPtMedium,
        fontWeight:'500',
       },
       container:{
        marginTop:10,
        backgroundColor:'#fff',
       },
       text1:{
           fontSize:18,
           fontWeight:'bold',
           color:'black',
           fontFamily:futuraPtMedium
       },
       line1Text:{
           flexDirection:'row',
       color:'#4A4646',
           marginTop:20

       }

});
export default styles;
