import React from 'react';
import { Text, Image, View, TouchableOpacity, Linking, Dimensions } from 'react-native';
import { createMaterialTopTabNavigator, createDrawerNavigator } from 'react-navigation';
import MapScreen from './screens/TabBarScreen/MapScreen'
import Help from './screens/SideMenuScreen/Help';
import InboxScreen from './screens/SideMenuScreen/InboxScreen';
import MyAccountScreen from './screens/MyAccountScreen';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon4 from 'react-native-vector-icons/FontAwesome5';
import RewardsScreen from './screens/SideMenuScreen/RewardsScreen';
import ExchangeAndReturn from './screens/SideMenuScreen/ExchangeAndReturn'
import Privacy from './screens/SideMenuScreen/Privacy'
import CompanyProfile from './screens/SideMenuScreen/CompanyProfile'
import Terms from './screens/SideMenuScreen/Terms'
import Membership from './screens/SideMenuScreen/Membership'
import StoreLocationListView1 from './screens/TabBarScreen/StoreLocationListView1';
import { Badge } from 'react-native-elements'

import config from '../package.json'
import { futuraPtMedium, futuraPtBook } from './styles/styleText'

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

import AppStack from './AppStack.js'

const TabLocation = createMaterialTopTabNavigator({
    StoreLocationListView1: {
        screen: StoreLocationListView1,
        navigationOptions: (navigation) => ({
            tabBarLabel: 'LIST VIEW',
            headerLeft: null,

        })
    },
    MapScreen: {
        screen: MapScreen,
        navigationOptions: (navigation) => ({

            tabBarLabel: 'MAP VIEW',
            headerLeft: null,

        })
    },
}, {
        tabBarOptions: {
            style: {
                backgroundColor: '#fff',
                height: 50,
                borderTopColor: 'transparent',
                borderTopWidth: 1,
                paddingRight: 10,
                paddingLeft: 10,
                borderTopWidth: 1,
            },
            labelStyle: { fontSize: 16, fontFamily: futuraPtMedium },
            indicatorStyle: {
                backgroundColor: '#ff4da6', // color of the indicator
                height: 4,
            },
            activeTintColor: 'black',
            inactiveTintColor: 'gray',
            activeBackgroundColor: 'white',
            inactiveBackgroundColor: '#F6F6F6',
            tabBarPosition: "top",
        }
    })

export default Drawer = createDrawerNavigator({
    RootStack: {
        screen: AppStack,
        navigationOptions: {
            drawerLabel: <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                <View style={{ height: 160, backgroundColor: 'black', marginTop: -10 }} >
                    <Image style={{
                        marginLeft: 70,
                        marginRight: 80,
                        marginTop: 40
                    }} source={require('./assets/base3.png')} />
                </View>
            </View>
        }
    },
    MyAccountScreen: {
        screen: MyAccountScreen,
        navigationOptions: {
            drawerLabel:
            <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: width / 1, paddingHorizontal: 10, paddingTop: 25, alignItems: 'center' }}>
                <View style={{ alignItems:'center', width: 40}}>
                    <Icon4 name="user-circle" color='black' size={25} />
                </View>
                <Text style={{ fontSize: 16, fontFamily: futuraPtMedium, fontWeight: '300', color: '#000000' - 87, marginLeft: 10 }}>MY ACCOUNT</Text>
            </View>
        }
    },
    InboxScreen: {
        screen: InboxScreen,
    },
    SelectStoreScreen: {
        screen: TabLocation,
        navigationOptions: {
            drawerLabel:
                <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: width / 1, paddingHorizontal: 10, paddingTop: 25, alignItems: 'center' }}>
                    <View style={{ alignItems:'center', width: 40}}>
                        <Icon3 name="map-marker-outline" color='black' size={30} />
                    </View>
                    <Text style={{ fontSize: 16, fontFamily: futuraPtMedium, fontWeight: '300', color: '#000000' - 87, marginLeft: 10 }}>LOCATION</Text>
                </View>
        }
    },
    // RewardsScreen: {
    //     screen: RewardsScreen,
    //     navigationOptions: {
    //         drawerLabel:
    //             <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: width / 1 }}>
    //                 <Icon3 style={{ marginTop: 20, marginLeft: 20 }} name="wallet-giftcard" color='black' size={27}>
    //                 </Icon3>
    //                 <Text style={{ fontSize: 16, fontFamily: futuraPtMedium, fontWeight: '300', marginTop: 25, color: '#000000' - 87, marginLeft: 20, }}>REWARDS</Text>
    //             </View>
    //     }
    // },
    Help: {
        screen: Help,
        navigationOptions: {
            drawerLabel:
                <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: width / 1, paddingHorizontal: 10, paddingVertical: 25, alignItems: 'center' }}>
                    <View style={{ alignItems:'center', width: 40}}>
                        <Icon3 name="information-outline" color='black' size={25} />
                    </View>
                    <Text style={{ fontSize: 16, fontFamily: futuraPtMedium, fontWeight: '300', color: '#000000' - 87, marginLeft: 10 }}>HELP & FAQS</Text>
                </View>
        }
    },
    ExchangeAndReturn: {
        screen: ExchangeAndReturn,
        navigationOptions: {
            drawerLabel:
                <View style={{ flexDirection: 'column', backgroundColor: '#fff', marginTop: 10, width: 400, }}><Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 20, color: 'lightgrey', }}>ABOUT SASA</Text>
                    <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>EXCHANGE AND RETURN</Text>
               </View>
        }
    },
    Terms: {
        screen: Terms,
        navigationOptions: {
            drawerLabel:
                <View style={{ flexDirection: 'column', backgroundColor: '#fff', width: 400, }}>
                    <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>TERMS & CONDITIONS</Text>
                </View>
        }
    },
    Privacy: {
        screen: Privacy,
        navigationOptions: {
            drawerLabel:
                <View style={{ flexDirection: 'column', backgroundColor: '#fff', width: 400, }}>
                    <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>PRIVACY POLICY</Text>
                </View>
        }
    },
    Membership: {
        screen: Membership,
        navigationOptions: {
            drawerLabel:
                <View style={{ flexDirection: 'column', backgroundColor: '#fff', width: 400, }}>
                    <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>MEMBERSHIP AND BENIFITS</Text>
                </View>
        }
    },
    CompanyProfile: {
        screen: CompanyProfile,
        navigationOptions: {
            drawerLabel:
                <View style={{ flexDirection: 'column', backgroundColor: '#fff', width: 400, paddingBottom: 10, marginBottom: 10 }}>
                    <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>COMPANY PROFILE</Text>
                </View>
        }
    },
    FollowUs: {
        screen: CompanyProfile,
        navigationOptions: {
            drawerLabel:
                <View style={{ flexDirection: 'column', backgroundColor: "#fff", width: 400 }}>
                    <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 20, color: 'lightgrey' }}>FOLLOW US</Text>
                    <TouchableOpacity onPress={() => Linking.openURL('https://web.facebook.com/SaSaMalaysia/')}>
                        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10 }}>
                            <View style={{ alignItems:'center', width: 40}}>
                                <Icon4 style={{ marginTop: 10 }} name="facebook" color='black' size={30} />
                            </View>
                            <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', color: '#000000' - 87, marginTop: 15 }}>FACEBOOK</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => Linking.openURL('https://www.instagram.com/sasamalaysia/')}>
                        <View style={{ flexDirection: 'row', marginLeft: 20 }}>
                            <View style={{ alignItems:'center', width: 40}}>
                                <Icon3 style={{ marginTop: 20 }} name="instagram" color='black' size={30} />
                            </View>
                            <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', color: '#000000' - 87, marginTop: 25, marginBottom: 20 }}>INSTAGRAM</Text>
                        </View>
                    </TouchableOpacity>
                </View>
        }
    },
    TellUs: {
        screen: CompanyProfile,
        navigationOptions: {
            drawerLabel:
                <View>
                    <View style={{ flexDirection: 'column', marginTop: 10, backgroundColor: '#fff', width: 400 }}><Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 20, color: 'lightgrey' }}>TELL US WHAT YOU THINK</Text>
                        <TouchableOpacity onPress={() => Linking.openURL('http://www.sasa.com.my/contact-us')}>
                            <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>CONTACT US</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => Linking.openURL('https://play.google.com/store')}>
                            <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 30, color: '#000000' - 87, marginBottom: 20 }}>RATE THE APP</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ backgroundColor: '#fff', width: 400, marginTop: 10}}>
                        <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 20, color: 'lightgrey', marginBottom: 20 }}>APP VERSION {config.version}</Text>
                    </View>
                </View>

        }
    },
    // AppVersion: {
    //     screen: CompanyProfile,
    //     navigationOptions: {
    //         drawerLabel:
    //             <View style={{ backgroundColor: '#fff', width: 400, marginTop: 10, backgroundColor:'red' }}>
    //                 <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: futuraPtBook, fontWeight: '300', marginTop: 20, color: 'lightgrey', marginBottom: 20 }}>APP VERSION {config.version}</Text>
    //             </View>
    //     }
    // },

}, {
        style: {
            backgroundColor: '#F5F5F5'
        }
    }
);
