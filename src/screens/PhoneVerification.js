


import { createStackNavigator, StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import { Col, Row, Grid } from "react-native-easy-grid";
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import { Container, Header, Content, Button, ListItem, List } from 'native-base';
import { futuraPtMedium, futuraPtBook } from '../styles/styleText'
const dimWidth = Dimensions.get('window').width
const dimHeight = Dimensions.get('window').height

export default class PhoneVerification extends Component {
    static navigationOptions = ({
        title: 'Phone Verification',
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily:futuraPtMedium,
            fontWeight:'500'
        },
    })

    constructor(props) {
        super(props);
        this.state = {
            check: true,
            selectedIndex: 0,
            oneText: '',
            oneTextError: false,
            twoText: '',
            threeText: '',
            fourText: '',
            fiveText: '',
            sixText: ''

        }
    }


    validation = () => {

        this.setState({
            oneTextError: false

        })


        if (this.state.oneText == '' || this.state.twoText == '' || this.state.threeText == '' || this.state.fourText == '' || this.state.fiveText == '' || this.state.sixText == '') {
            this.setState({
                oneTextError: true
            })
        }

       else {
            this.props.navigation.navigate('Activate1')
        }
     }

     render() {
        const { navigation } = this.props;
        const contactNo = navigation.getParam('ContactNo');
        return (
            <ScrollView style={{flex:1,backgroundColor:'#fff'}} >

               <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{color:'#000', justifyContent: "center", alignItems: 'center',fontWeight:'300',fontFamily:futuraPtBook ,fontSize:18}} >
                        We sent you a code to verify
                </Text>
                </View>


                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 3, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center',fontWeight:'300',fontFamily:futuraPtBook ,fontSize:18 }} >
                        your phone number.
                </Text>
                </View>

                {/******************* Text 2 ************************* */}
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 10, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center' ,fontFamily:futuraPtBook,fontWeight:'300',fontSize:16}} >
                        Sent to
                </Text>
                </View>



                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: -3, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center',fontFamily:futuraPtMedium,fontWeight:'500',fontSize:16 }} >
                        +60{contactNo}
                </Text>
                </View>

                {/********************* Text 3 *********************** */}

                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1}  returnKeyType = {"next"}style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='1'
                            onChangeText={oneText => this.setState({ oneText })}
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>





                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1} returnKeyType = {"next"} style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='2'
                            onChangeText={twoText => this.setState({ twoText })}
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1} returnKeyType = {"next"} style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='3'
                            onChangeText={threeText => this.setState({ threeText })}
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1} returnKeyType = {"next"} style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='4'
                            onChangeText={fourText => this.setState({ fourText })}
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1}  returnKeyType = {"next"}style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='5'
                            onChangeText={fiveText => this.setState({ fiveText })}
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1} returnKeyType = {"next"} style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='6'
                            onChangeText={sixText => this.setState({ sixText })}
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>

                </View>

                {this.state.oneTextError == true &&
                    <View>
                        <Text style={{ color: 'red', fontSize: 16, marginLeft: 15, marginTop: 10 ,fontFamily:futuraPtBook,}}> Please enter OTP </Text>
                    </View>
                }

                {/******************* Text 4 ************************* */}


                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 24, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center',fontFamily:futuraPtBook,fontSize:16,fontWeight:'300' }} >
                        I didn't receive a
                </Text>
                </View>


                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 6, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center' }} >
                        code!
                </Text>
                </View>

                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 6, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#ff4da6', justifyContent: "center", alignItems: 'center', fontSize: 18,fontFamily:futuraPtMedium }} >
                        Resend
                </Text>
                </View>

                {/************************ Submit ******************** */}
                <View style={{ marginTop: 20, height: 50 ,width:'100%'}}>
                    <TouchableOpacity style={{ backgroundColor: '#E4007C', justifyContent: "center", alignItems: 'center', height: 50 }}
                        onPress={this.validation} >
                        <Text style={{ color: 'white', fontSize: 20,fontFamily:futuraPtMedium }}>SUBMIT</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
