import React, { Component } from 'react';
import styles from '../styles/styleMyAccount'
import { Text, View, Image, ScrollView, TouchableOpacity, AsyncStorage, Dimensions } from 'react-native';
import { ListItem, List, Left, Right, } from 'native-base';
import Dialog, { DialogContent, DialogButton, SlideAnimation } from 'react-native-popup-dialog';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon2 from 'react-native-vector-icons/AntDesign';
import Barcode from 'react-native-barcode-builder';
import Moment from 'moment';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../config/Service';
import CanonicalPath from '../config/CanonicalPath';
import Circle from '../components/Circle'
import { futuraPtMedium, futuraPtBook } from '../styles/styleText'

const messages = {
    MLVIP1: "Spend another RMXX to renew your VIP!NK Membership",
    MLVIP2: "Spend another RMXX to renew your VIP!NK Membership",
    MLVIP3: "Spend another RMXX to upgrade to VIP!NK Membership"
}

const contents = {
    MLVIP1: [
        "Pay with Points (200 Points = RM1)",
        "Collect 1 Points with every RM1 spent",
        "Enjoy additional discount 5% OFF",
        "Get RM50 voucher on your birthday month"
    ],
    MLVIP2: [
        "Pay with Points (200 Points = RM1)",
        "Collect 1 Points with every RM1 spent",
        "Enjoy additional discount 5% OFF",
        "Get RM50 voucher on your birthday month"
    ],
    MLVIP3: [
        "Collect 1 Points with every RM1 spent",
        "Pay with Points (200 Points = RM1)",
        "Get upgraded to VIP!NK Membership by spending an additional RM200 within 3 months"
    ]
}

const icon = {
    MLVIP1: require('../assets/reddem-VIPink.png'),
    MLVIP2: require('../assets/reddem-VIPink.png'),
    MLVIP3: require('../assets/reddem.png')
}

const {height, width} = Dimensions.get('window')

export default class MyAccountScreen extends Component {

    static navigationOptions = ({
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff',

        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium,
            fontWeight: '500'
        },
    })

    constructor(props) {
        super(props);
        this.state = {
            check: true,
            selectedIndex: 0,
            isVisible: false,
            card: '',
            points: 0,
            circle_percent: 0,
            remain_spending: "",
            name: '',
            expired_date: '',
            member_type: ""
        }
    }

    componentDidMount() {
        this.get_detail()
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });
        console.log('onSuccess ', filter, json)
        switch (filter) {
            case "GET_DETAIL":

                var status = json[0].status;
                if (status == 1) {

                    this.setState({
                        card: json[0].card,
                        points: json[0].points,
                        circle_percent: json[0].circle_percent,
                        name: json[0].name,
                        expired_date: json[0].expired_date,
                        member_type: json[0].member_type,
                        remain_spending: json[0].remain_spending
                    })

                } else {
                    Alert.alert(json[0].error)
                }

                break
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        alert(e)
    }

    async get_detail() {
        await this.setStateAsync({ isLoading: true });

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: {},
            canonicalPath: CanonicalPath.GET_DETAIL
        };

        await Service.request(this.onSuccess, this.onError, "GET_DETAIL", params);
    }

    
    onLogOut = async () => {
        await AsyncStorage.clear();
        this.props.navigation.navigate('Login')
    }

    renderContent(typeMember){
        const contentByMember = contents[typeMember];

        return(
            <View style={[styles.container, {padding:10}]}>
                <Text style={styles.text1}> VIP!INK Membership Benifits </Text>
                {
                    contentByMember && contentByMember.map((content, iter) => {
                        return(
                            <View style={[styles.line1Text, {alignItems:'flex-start'}]}>
                                <Text style={{ fontSize: 15, marginRight: 5 }}> {iter+1}</Text>
                                <Text style={{ fontSize: 16, color: '#4A4646', fontFamily: futuraPtBook }}>{content}</Text>
                            </View>
                        )
                    })
                }
            </View>
        )
    }

    renderCard(expireDate){

        const cardNo = this.state.card.trim();
        const hasCard = ( cardNo != "" && cardNo != 'none' );

        return(
            <View>
                <View style={styles.optionViewOne}>
                    <View style={styles.viewTextTopOne}>
                        {/* <Image style={{marginTop:5}}source={require('../assets/cardno.png')}></Image> */}
                        <Icon name="credit-card" size={20} color="black" />
                    </View>
                    <View style={styles.viewTextTopOne}>
                        <Text style={styles.textHeading}>CARD No { hasCard ? cardNo : "NIL" }</Text>
                        <Text style={styles.textNormal}>Expire on {expireDate}</Text>
                    </View>

                </View>
                {
                    hasCard ?
                    <View style={{ flex: 1, paddingLeft: 50, paddingRight: 40 }} >
                        <Barcode width={1.6} height={50} value={ cardNo } format="CODE128" />
                    </View>
                    :
                    null
                }

                <View style={styles.lineDown}></View>
            </View>
        )
    }

    renderMessageMember( typeMember, remain_spending, circle_percent ){

        const message = messages[ typeMember ];

        return(
            <TouchableOpacity
                style={{justifyContent:'center', alignItems:'center', padding:10}}
                onPress={() => {
                    this.setState({ visible: true });
                }}
            >
                <Image
                    source={ icon[ typeMember ] }
                    resizeMode="contain"
                    style={{ height: (width/4), width: (width/4) }}/>
                <Text style={ styles.textMessage }>
                    { message ? message.replace( "XX", remain_spending ) : message }
                </Text>
            </TouchableOpacity>
        )
    }

    render() {
        var dateString = this.state.expired_date;
        var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
        var expireDate = momentObj.format('DD MMMM YYYY');
        const { member_type, remain_spending, circle_percent } = this.state;
        // const typeMember = "MLVIP1";

        return (
            <ScrollView style={styles.containerBackground}>

                <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />
                <View style={styles.viewText}>
                    <Text style={styles.textName}>Hi, {this.state.name}!</Text>
                    <View style={{marginVertical:20}}>
                        {
                            member_type
                            ? <Circle size={(width/1.5)} thick={(width/8)} color1="#ffebee" color2="#e6007b"
                                    content={ () => this.renderMessageMember( member_type, remain_spending, circle_percent )}
                                />
                            : null
                        }
                    </View>
                </View>


                <Dialog style={{height: 150, width: 200}}
                    visible={this.state.visible}
                    dialogStyle={{marginHorizontal:30}}
                    dialogAnimation={new SlideAnimation({
                        slideFrom: 'bottom',
                    })}
                    rounded={false}
                    actions={[
                        <DialogButton
                            text=""
                            onPress={() => { }}
                        />,
                        <DialogButton
                            text="CLOSE"
                            textStyle={
                                color = 'red'
                            }
                            onPress={() => {
                                this.setState({ visible: false });
                            }}
                        />,
                    ]}
                    onTouchOutside={() => {
                        this.setState({ visible: false });
                    }}
                >
                    <DialogContent>
                        { this.renderContent( member_type )}
                    </DialogContent>
                </Dialog>

                <View style={styles.blankView}></View>

                {/**************************************** Card ****************************************************** */}
                { this.renderCard(expireDate) }
                {/********************************************************************************************* */}

                {/**************************************** POINT  ****************************************************** */}
                <View style={styles.optionViewOne}>
                    <View style={styles.viewTextTopOne}>
                        <Icon name="dollar" size={20} color="black" />
                    </View>
                    <View style={styles.viewTextTopOne}>
                        <Text style={styles.textHeading}>POINT BALANCE: {this.state.points}</Text>
                        <Text style={styles.textNormal}>POINT BALANCE WILL UPDATE EVERY 24 HOURS AFTER TRANSACTION.</Text>
                    </View>
                </View>
                <View style={styles.blankView}></View>
                {/********************************************************************************************* */}

                <List>
                    <ListItem selected>
                        <Left>
                            {/**************************************** TRANSACTIONS  ****************************************************** */}
                            <TouchableOpacity style={styles.optionView} onPress={() => this.props.navigation.navigate('OnlinePurchaseTransaction')}>

                                <Image style={styles.viewTextTop} source={require('../assets/basket.png')}></Image>
                                <View style={styles.viewTextTop}>
                                    <Text style={styles.textHeading}>MY TRANSACTIONS</Text>
                                </View>
                            </TouchableOpacity>
                        </Left>
                        <Right>
                            <Icon name="angle-right" size={25} color="lightgrey" />
                        </Right>
                    </ListItem>
                    <ListItem>
                        <Left>
                            {/**************************************** PRODUCTS  ****************************************************** */}
                            <View style={styles.optionView}>
                                <Icon2 name="hearto" size={20} color="black" />
                                {/* <Image style={styles.viewTextTop} source={require('../assets/like.png')}></Image> */}
                                <TouchableOpacity style={styles.viewTextTop} onPress={() => this.props.navigation.navigate('LikedProducts')}>
                                    <Text style={styles.textHeading}>LIKED PRODUCTS</Text>
                                </TouchableOpacity>
                            </View>
                        </Left>
                        <Right>
                            <Icon name="angle-right" size={25} color="lightgrey" />
                        </Right>
                    </ListItem>

                    <ListItem >
                        <Left>

                            {/**************************************** REVIEWS  ****************************************************** */}
                            <TouchableOpacity style={styles.optionView} style={styles.optionView} onPress={() => this.props.navigation.navigate('MyReviewsScreen')}>

                                <Image style={styles.viewTextTop} source={require('../assets/review.png')}></Image>
                                <View style={styles.viewTextTop}>
                                    <Text style={styles.textHeading}>MY REVIEWS</Text>
                                </View>
                            </TouchableOpacity>
                        </Left>
                        <Right>
                            <Icon name="angle-right" size={25} color="lightgrey" />
                        </Right>
                    </ListItem>
                    <ListItem >
                        <Left>
                            {/**************************************** CHANGE PASSWORD  ****************************************************** */}
                            <TouchableOpacity style={styles.optionView} onPress={() => this.props.navigation.navigate('ChangePassWord1')}>
                                <Icon1 name="lock-outline" size={25} color="black" />
                                {/* <Image style={styles.viewTextTop} source={require('../assets/lock.png')}></Image> */}
                                <View style={styles.viewTextTop}>
                                    <Text style={styles.textHeading}>CHANGE PASSWORD</Text>
                                </View>
                            </TouchableOpacity>
                        </Left>
                        <Right>
                            <Icon name="angle-right" size={25} color="lightgrey" />
                        </Right>
                    </ListItem>

                </List>

                {/**************************************** LOGOUT  ****************************************************** */}
                <View style={styles.blankView}></View>
                <TouchableOpacity style={styles.viewText} onPress={this.onLogOut}>

                    <Text style={styles.textLogout}>LOGOUT</Text>

                </TouchableOpacity>
                <View style={styles.blankView}></View>
                {/********************************************************************************************* */}

            </ScrollView>
        );
    }
}
