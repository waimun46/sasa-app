import React, { Component } from 'react';
import { View, Image, AsyncStorage, ImageBackground, Dimensions } from 'react-native';
import Config from '../config/Config';
import OneSignal from 'react-native-onesignal';

export default class SplashScreen extends Component {
    static navigationOptions = {

        header: null,
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    componentWillMount() {
        setTimeout(() => {
            // this.props.navigation.navigate('Login')
            this.checkUserToken()

        }, 3000);

        // OneSignal.init(Config.oneSignalAppId, {kOSSettingsKeyAutoPrompt : true});
    }

    async checkUserToken() {
        const userToken = await AsyncStorage.getItem('access_token');

        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        this.props.navigation.navigate(userToken ? 'App' : 'Auth');
    }

    render() {
        return (
            <ImageBackground style={{ width: '100%', height: '100%' }} source={require('../assets/splashBg.png')} >
                <View style={{flex: 1, justifyContent: "center", alignItems: 'center',  backgroundColor:'#000000'}}>
                    {/* <Image source={require('../assets/sasa-launch.gif')}> */}
                    <Image source={require('../assets/Sasa-Scattering-Animation.gif')}>
                    </Image>
                </View>
            </ImageBackground>
        )
    }
}
