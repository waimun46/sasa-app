import React, { Component } from 'react';
import Dialog, { DialogContent, DialogButton, SlideAnimation } from 'react-native-popup-dialog';
import Barcode from 'react-native-barcode-builder';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions, Alert } from 'react-native';
import { Container, Header, Content, Button, ListItem, List } from 'native-base';
import Moment from 'moment';
import HTML from 'react-native-render-html';
import { futuraPtMedium, futuraPtBook } from '../styles/styleText'

export default class BirthdayVoucher extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.item.title}`,
        headerStyle: {
            backgroundColor: 'black',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium
        },
    })

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            item: {},
            isPayment: false
        }

    }

    componentDidMount() {
        this.setState({
            item: this.props.navigation.getParam('item'),
            isPayment: this.props.navigation.getParam('isPayment')
        })
    }

    redemPress = () => {
        Alert.alert(
            '',
            'Are you at store cashier now? ',
            [

                { text: 'Cancel', },
                { text: 'YES', onPress: () => this.showDialog() },
            ],
            { cancelable: false }
        )
    }

    showDialog() {
        console.log('showDialog ', this.state.item )
        this.setState({
            isVisible: true,
        });
    }

    render() {

        var dateString = this.state.item.valid_date;
        var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
        var momentString = momentObj.format('DD MMMM YYYY');

        return (
            <View style={styles.container}>
                <View style={styles.text1view}>
                    <Text style={styles.text1}>{this.state.item.title}</Text>
                    <Text style={{ fontFamily: futuraPtBook, fontWeight: '300', fontSize: 16, marginTop: 10 }} >Valid untill {momentString}</Text>
                </View>
                <View style={styles.img}>
                    <Image style={{ width: '90%', height: 200, resizeMode: 'contain' }} source={{ uri: this.state.item.image_url }} />
                    <View style={styles.text2}>
                        <HTML html={this.state.item.description} />
                    </View>
                    {/* <Text style={styles.text2}>{this.state.item.description}</Text> */}
                </View>
                {
                    this.state.isPayment
                    ?   <View style={styles.trContainer}>
                            <TouchableOpacity onPress={() => this.redemPress()} style={styles.takereward}  >
                                <Text style={styles.trText}>USE NOW </Text>
                            </TouchableOpacity>
                            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')} style={styles.takereward}  >
                                <Text style={styles.trText}>USE VOUCHER </Text>
                            </TouchableOpacity> */}
                        </View>
                    :   <View style={{ margin: 20, padding: 10, borderWidth: 1, justifyContent:'center', alignItems:'center'}}>
                            <Text >You may use this voucher during checkout</Text>
                        </View>
                }
                <Dialog
                    visible={this.state.isVisible}
                    dialogAnimation={new SlideAnimation({
                        slideFrom: 'bottom',
                    })}
                    rounded={false}
                    width={300}
                    height={420}
                    onTouchOutside={() => {
                        this.setState({ isVisible: false });
                    }}
                >
                    <DialogContent>
                        <View style={{ backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ width: 300, height: 160 }} source={{ uri: this.state.item.image_url }} />
                            <Text style={[styles.text2, { textAlign: 'center', marginBottom: 5 }]}>{this.state.item.title}</Text>
                            <View style={{
                                height: 100,
                                width: 240, borderWidth: 1,
                                borderColor: "#ddd",
                                justifyContent: 'center', alignItems: 'center',
                            }}>
                                <Text style={[styles.text2, { color: '#ddd' }]}>VOUCHER CODE </Text>
                                <Text style={[styles.text2, { color: '#E4007C', fontSize: 27, marginTop: 0 }]}>{this.state.item.voucher_code}</Text>
                                <Text style={[styles.text2, { color: '#ddd', fontSize: 16, marginTop: 0, marginBottom: 10 }]}>valid through {momentString} </Text>
                            </View>

                            <View style={{ paddingLeft: 10, paddingRight: 10 }} >
                                <Barcode width={1.6} height={50} value="Hello World" format="CODE128" />
                            </View>
                            {/* <Text style={[styles.text2, { textAlign: 'center', marginTop: 0 }]}>(10) ABC123(21)0001! </Text> */}

                        </View>
                    </DialogContent>
                </Dialog>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,

    },
    text1view: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        marginTop: 20
    },
    text1: {
        fontSize: 22,
        fontFamily: futuraPtMedium,
        fontWeight: '300',

        color: '#111111'
    },
    img: {
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        marginLeft: 10

    },
    text2: {
        marginRight: 20,
        marginLeft: 20,
        marginTop: 10,
        color: 'black',
        fontSize: 18,
        fontFamily: futuraPtBook,
        fontWeight: '300'
    },
    trContainer: {
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,

        height: 50
    },
    takereward: {
        backgroundColor: '#E4007C',
        justifyContent: "center",
        alignItems: 'center',
        height: 50,
        marginBottom: 100,
    },
    trText: {
        color: 'white',
        fontSize: 20,
        fontFamily: futuraPtMedium
    }

})
