import React, { Component } from 'react';
import { Text, View, Image, ScrollView, TouchableOpacity, FlatList, StatusBar } from 'react-native';
import Moment from 'moment';
import styles from '../../styles/styleEvoucher'

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
import { futuraPtMedium } from '../../styles/styleText'

export default class PromotionsList extends Component {

    static navigationOptions = ({
        title: 'Promotions',
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black',

        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium,
            fontWeight: '500'
        },
    })
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            isHaveRecord: false,
            promotions: [],
        }

    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "PROMOTIONS":

                if (json.length > 0) {
                    this.setState({
                        isHaveRecord: true,
                        promotions: json,
                    })
                } else {
                    this.setState({
                        isHaveRecord: false,
                    })
                }

                break
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        alert(e)
    }

    async promotions() {
        await this.setStateAsync({ isLoading: true });

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: {},
            canonicalPath: CanonicalPath.PROMOTIONS
        };

        await Service.request(this.onSuccess, this.onError, "PROMOTIONS", params);
    }

    componentDidMount() {
        this.promotions()
    }

    renderRow = (item) => {

        var dateString = item.item.expiry_date;
        var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
        var momentString = momentObj.format('DD MMMM YYYY');

        return (
            <View style={styles.styleCard}>
                <TouchableOpacity style={{ justifyContent: "center", alignItems: 'center', }}
                    onPress={() =>
                        (item.item.type === 4 ?
                            this.props.navigation.navigate('TrickNdTreat', { data: item.item })
                            :
                            this.props.navigation.navigate('DetailPromotion', { data: item.item }))
                    }>
                    {
                        (item.item.image_url === "") ?
                            <Image style={{ width: '100%', height: 155, resizeMode: 'contain' }} source={require('../../assets/image_not_found.png')} />
                            :
                            <Image style={{ width: '100%', height: 155, resizeMode: 'contain' }} source={{ uri: item.item.image_url }} />
                    }


                </TouchableOpacity>

                <View style={{ flex: 1, flexDirection: 'row' }}>

                    <View style={{ flex: 1, flexDirection: 'column' }}>

                        <Text style={styles.textHeading} >{item.item.title}</Text>
                        <Text style={styles.textNormal} >Valid until {momentString}</Text>
                    </View>

                    <View>

                    </View>
                </View>
            </View>
        )
    }

    render() {
        return (
            <ScrollView style={styles.containerBackground}>
                <StatusBar
                    backgroundColor="black"
                    barStyle="light-content"
                />

                <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />

                {
                    (this.state.isHaveRecord) ?
                        <FlatList
                            vertical
                            data={this.state.promotions}
                            renderItem={item => this.renderRow(item)}>
                        </FlatList>
                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                            <Text>No Record</Text>
                        </View>

                }

            </ScrollView>
        );
    }
}
