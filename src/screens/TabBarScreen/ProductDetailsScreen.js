
import React, { Component } from 'react';
import { Alert, Picker, Text, View, Image, ScrollView, TextInput, TouchableOpacity, FlatList, Dimensions, AsyncStorage, List } from 'react-native';
import styles from '../../styles/styleProductDetails';
import StarRating from 'react-native-star-rating';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Dialog, { DialogContent, DialogButton, SlideAnimation } from 'react-native-popup-dialog';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Icon2 from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/Foundation';

import Moment from 'moment';
import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
// import Reviews from './product/detail/Reviews'
import HTMLView from '../../components/HTMLView'
import { futuraPtMedium, futuraPtBook } from '../../styles/styleText'

const backgroundColor = '#fff'

export class Reviews extends Component {

  constructor(props) {
    super(props);
    this.state = {
      item: {},
      reviews: [],

    }
  }

  componentDidMount() {
    this.reviews()

  }
  onSuccess = async (filter, json) => {

    switch (filter) {
      case "REVIEWS":

        if(json[0].status == 0) return;

        this.setState({
          reviews: json
        })

        break
    }

  }

  onError = (filter, e) => {

    Alert.alert(e)
  }

  async reviews() {

    var data = await {
      "product_id": this.props.product_id
    }

    var params = await {
      contentType: "multipart/form-data",
      Accept: "application/json",
      keepAlive: true,
      method: 'GET',
      useToken: true,
      data: data,
      canonicalPath: CanonicalPath.REVIEWS
    };

    await Service.request(this.onSuccess, this.onError, "REVIEWS", params);
  }

  renderReviews = (item) => {
    var dateString = item.item.date;
    var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
    var date = momentObj.format('DD MMMM YYYY');
    return (
      <View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 10, marginRight: 10, marginTop: 20 }}>
          <Text style={{ fontSize: 18, fontWeight: '300', color: '#000', fontFamily: 'futura-pt-book' }}>{item.item.name} </Text>
          <Text style={{ fontSize: 16, marginLeft: 80, fontFamily: 'futura-pt-book' }}>{date} </Text>
        </View>
        <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10 }}>
          <Text style={{ fontFamily: 'futura-pt-book', fontSize: 16 }} >{item.item.comment} </Text>
        </View>
        <View style={{ width: 60, marginLeft: 10, marginTop: 10 }}>
          <StarRating
            disabled={false}
            maxStars={5}
            rating={item.item.rating}
            starSize={20}
            fullStarColor={'#DAA520'}
          />
        </View>
        <View style={{ height: 2, marginHorizontal: 10, backgroundColor: '#ddd', marginTop: 10 }} />
      </View>
    )
  }

  render() {
    return (
      <View style={{ backgroundColor: '#fff', }} >
        {
          (this.state.reviews.length > 0) ?
            <FlatList
              vertical
              data={this.state.reviews}
              renderItem={item => this.renderReviews(item)}>
            </FlatList>
            // <List dataArray={this.state.reviews}
            //   renderRow={item => this.renderReviews(item)}>
            // </List>
            :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'column', marginTop: 30 }}>
              <Text>No Record</Text>
            </View>

        }

      </View>
    )
  }
}

export default class ProductDetailsScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.item.product_name}`,
    headerStyle: {
      backgroundColor: 'black',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 22,
      fontFamily: futuraPtMedium
    },
  })

  constructor(props) {
    super(props);
    this.state = {
      item: {},
      cartList: [],
      index: 0,
      routes: [
        { key: 'first', title: 'DETAIL' },
        { key: 'second', title: 'HOW TO USE' },
        { key: 'third', title: 'REVIEWS' },
      ],
      isVisible: false,
      heart: false,
      image: '',
      name: '',
      productDetails: [],
      subTotal: 0,
      listImage: [],
      activeSlide: 0,
      like: 0,
    }
  }

  componentDidMount() {
    // var data = this.props.navigation.getParam('item')
    // if(data.like == 1){
    //   this.setState({
    //     heart: true,
    //   })
    // }else{
    //   this.setState({
    //     heart: false,
    //   })
    // }

    this.setState({
      item: this.props.navigation.getParam('item')
    })
    this.ids()
  }

  _handleIndexChange = index => this.setState({ index });

  _keyExtractor = (item, index) => item.id;

  goBack = () => this.props.navigation.goBack();

  handleBackPress() {
    if (this.state.isVisible) {
      this.setState({ isVisible: false })
    } else {
      this.goBack()
    }
    return true
  }

  async addCart() {

    if (this.state.item.quantity == "0") {
      Alert.alert("Out of Stock")
      return;
    }

    var cartStorage = await AsyncStorage.getItem('cart')
    console.log('on addCart ', cartStorage)
    if (cartStorage != null) {
      var cart = JSON.parse(cartStorage)
      for (var i = 0; i < cart.length; i++) {
        if (cart[i].id == this.state.item.id) {
          this.setState({
            cartList: cart,
            isVisible: true,
          });
          this.subTotal()
          return;
        }
      }
    } else {
      var cart = [];
    }

    var item = Object.assign({}, this.state.item);
    item.amount = 1;
    item.subTotal = ((parseFloat(item.offer_price)) == 0 ? item.price : item.offer_price);
    item.image_url = item.image_url[0];

    cart.push(item)
    await AsyncStorage.setItem('cart', JSON.stringify(cart))

    this.setState({
      cartList: cart,
      item: item,
      isVisible: true,
    });

    this.subTotal()

  }

  showDialog(cart) {
    this.setState({
      isVisible: true,
      // listImage: []
    });

  }

  async deleteItem(index) {
    var array = [...this.state.cartList]; // make a separate copy of the array
    if (index !== -1) {
      array.splice(index, 1);
      this.setState({ cartList: array });
    }

    await AsyncStorage.setItem('cart', JSON.stringify(array))

    this.subTotal()
  }

  changeAmount = async (value, index) => {

    // if (value == "") value = 1

    const newArray = [...this.state.cartList];
    newArray[index].amount = value;
    var subTotalProduct = value * (parseFloat(newArray[index].offer_price) == 0 ? newArray[index].price : newArray[index].offer_price);
    newArray[index].subTotal = parseFloat(subTotalProduct).toFixed(2);
    this.setState({ cartList: newArray });

    await AsyncStorage.setItem('cart', JSON.stringify(newArray))

    this.subTotal()
  }

  subTotal() {
    var subTotal = 0
    for (var i = 0; i < this.state.cartList.length; i++) {
      if (this.state.cartList[i].offer_price != 0) {
        var total = subTotal + parseFloat(this.state.cartList[i].offer_price) * (this.state.cartList[i].amount ? this.state.cartList[i].amount : 1);
      } else {
        var total = subTotal + parseFloat(this.state.cartList[i].price) * (this.state.cartList[i].amount ? this.state.cartList[i].amount : 1);
      }
      subTotal = total;
    }
    this.setState({
      subTotal: parseFloat(subTotal).toFixed(2),
    })
  }

  onCheckout() {

    if (this.state.cartList.length < 1) {
      return;
    }

    if (this.state.subTotal < 25) {
      Alert.alert("Minimum Purchase of RM25 is required to checkout.")
      return;
    }

    for (var i = 0; i < this.state.cartList.length; i++) {
      if (this.state.cartList[i].amount === undefined || this.state.cartList[i].amount < 1) {
        Alert.alert("Please fill amount")
        return;
      }
      else if (this.state.cartList[i].amount > this.state.cartList[i].quantity) {
        Alert.alert(this.state.cartList[i].product_name + " Insufficient Stock")
        return;
      } else if (this.state.cartList[i].amount > 6) {
        Alert.alert(this.state.cartList[i].product_name + " Maximum Quantity Allowed Should be 6 Only")
        return;
      }
    }

    this.props.navigation.navigate('CheckoutScreen')
    this.setState({ isVisible: false })
  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  onSuccess = async (filter, json) => {
    this.setStateAsync({ isLoading: false });

    switch (filter) {
      case "UNLIKE_PRODUCT":
      case "LIKE_PRODUCT":
        this.setState({ heart: !this.state.heart })
        break
      case "IDS":
        const prod = json[0];
        console.log('on success ids ', prod)
        prod.subTotal = ((parseFloat(json[0].offer_price)) == 0 ? json[0].price : json[0].offer_price);

        var hearts = (json[0].like == 1) ? true : false;
        this.setState({
          listImage: json[0].image_url,
          item: prod,
          heart: hearts
        })
        break
    }

  }

  onError = (filter, e) => {
    this.setStateAsync({ isLoading: false });

    Alert.alert(e)
  }

  async ids() {
    await this.setStateAsync({ isLoading: true });

    var data = await {
      "product_id[0]": this.state.item.id
    }

    var params = await {
      contentType: "multipart/form-data",
      Accept: "application/json",
      keepAlive: true,
      method: 'GET',
      useToken: true,
      data: data,
      canonicalPath: CanonicalPath.IDS
    };

    await Service.request(this.onSuccess, this.onError, "IDS", params);
  }

  async likeProduct() {

    await this.setStateAsync({ isLoading: true });

    var formData = new FormData();

    formData.append('product_id', this.state.item.id);

    if (!this.state.heart) {
      var params = await {
        contentType: "multipart/form-data",
        Accept: "application/json",
        keepAlive: true,
        method: 'POST',
        useToken: true,
        data: formData,
        canonicalPath: CanonicalPath.LIKE_PRODUCT
      };

      await Service.request(this.onSuccess, this.onError, "LIKE_PRODUCT", params);
    } else {
      var params = await {
        contentType: "multipart/form-data",
        Accept: "application/json",
        keepAlive: true,
        method: 'POST',
        useToken: true,
        data: formData,
        canonicalPath: CanonicalPath.UNLIKE_PRODUCT
      };

      await Service.request(this.onSuccess, this.onError, "UNLIKE_PRODUCT", params);
    }

    // this.setState({ heart: !this.state.heart })
  }

  renderCart = ({ item, index }) => {

    let amountt = this.state.cartList[index].amount

    console.log("setan", amountt, item)
    return (
      <View style={{ flex: 1, flexDirection: 'row', }} >

        <Image style={{ width: 70, height: 80, marginTop: 7, borderWidth: 1, borderColor: 'grey' }} source={{ uri: item.image_url }}></Image>
        <View style={{ flex: 1, flexDirection: 'column', width: 200 }}>

          <Text numberOfLines={3} style={{ marginLeft: 4, marginTop: 4, fontSize: 16, fontFamily: futuraPtMedium, fontWeight: '500' }}>{item.product_name}</Text>
          <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, textDecorationLine: 'line-through', fontFamily: futuraPtBook, fontWeight: '300' }}>{(parseFloat(item.offer_price)) == 0 ? "" : "RM" + item.price}</Text>
          <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 18, color: '#ff4da6', fontFamily: futuraPtBook, fontWeight: '300' }}>{(parseFloat(item.offer_price)) == 0 ? "RM" + item.price : "RM" + item.offer_price} </Text>
        </View>

        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-end', }} >
          <TouchableOpacity style={{ marginRight: 10, marginTop: 5 }} onPress={() => this.deleteItem(index)}>
            <Icon1 name="trash" size={25} color="lightgrey" style={{}} />

          </TouchableOpacity>

          <View style={{ marginTop: 10, height: 40, width: 60, alignItems: 'flex-end', justifyContent: 'center' }} >
            <View style={{ flexDirection: 'row', }}>
              <TextInput
                style={{ height: 40, width: 40, borderColor: 'black', borderWidth: 0.5, padding: 10, }}
                keyboardType='numeric'
                onChangeText={(text) => this.changeAmount(text, index)}
                value={amountt}
                defaultValue="1"
              />
            </View>
          </View>

          <Text style={{ fontSize: 14, color: 'black', marginTop: 5, fontFamily: futuraPtMedium, fontWeight: '300' }}>
            RM {this.state.cartList[index].subTotal}
            {/* RM {(this.state.cartList[index].subTotal ? this.state.cartList[index].subTotal : ((parseFloat(item.offer_price)) == 0 ? item.price : item.offer_price))} */}
          </Text>
        </View>

      </View>)
  }

  renderSwiper({ item, index }) {
    return (
      <View style={{ backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('ItemsPictures',
          { image: item })}>
          <Image style={{ height: 250, width: 200, resizeMode: 'contain' }} source={{ uri: item }}></Image>
        </TouchableOpacity>
      </View>
    )
  }

  promotion() {
    const { extra_point, buy_free, promotion_type } = this.state.item;
    let promo = null;

    if (extra_point && extra_point != "NONE") { promo = extra_point }
    else if (buy_free && buy_free != "NONE") { promo = buy_free }
    else if (promotion_type && promotion_type != "NONE") { promo = promotion_type }

    console.log('on promotion ', this.state, promo);
    if (promo == null) { return null }
    else {
      return (
        <TouchableOpacity style={{ width: 100, height: 30, backgroundColor: '#F48FB1', justifyContent: "center", alignItems: 'center', marginLeft: 20, borderRadius: 15 }}>
          <Text style={{ color: '#fff', fontFamily: futuraPtBook }}>{promo}</Text>
        </TouchableOpacity>
      )
    }
  }

  render() {
    console.log('render product detail ', this.state.index)
    let bounds = Dimensions.get('window');
    let width = bounds.width - 80

    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
          <View style={{ backgroundColor: backgroundColor, flex: 1, }}>

            <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }}></Progressbar>

            {/******************** v 1  : Product Image ***************************** */}
            <View style={styles.container}>

              <View style={{ flex: 1, }}>

                <Carousel
                  ref={(c) => this._carousel = c}
                  data={this.state.listImage}
                  renderItem={this.renderSwiper.bind(this)}
                  sliderWidth={bounds.width}
                  sliderHeight={250}
                  itemWidth={width}
                  itemHeight={250}
                  onSnapToItem={(index) => this.setState({ activeSlide: index })}
                  loop={true}
                />
                <Pagination
                  style
                  dotsLength={this.state.listImage.length}
                  activeDotIndex={this.state.activeSlide}
                  containerStyle={{ backgroundColor: '#00000000', paddingTop: 0, paddingBottom: 0, marginTop: -20 }}
                  dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    backgroundColor: '#E4007C'
                  }}
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
                />

              </View>

              <View style={{ alignItems: 'flex-end', marginTop: -35 }}>
                <TouchableOpacity
                  style={{ width: 80, height: 80, borderRadius: 40, marginRight: 10, backgroundColor: '#fff', justifyContent: "center", alignItems: 'center', shadowColor: 'rgba(0,0,0, .4)', shadowOffset: { height: 1, width: 1 }, shadowOpacity: 1, shadowRadius: 1, elevation: 2, }}
                  onPress={() => this.likeProduct()} >
                  {this.state.heart ? <Icon name="heart" size={30} color="#E4007C" /> : <Icon2 name="hearto" size={30} color="grey" />}
                </TouchableOpacity>

              </View>

            </View>

            <Dialog
              visible={this.state.isVisible}
              dialogAnimation={new SlideAnimation({
                slideFrom: 'bottom',
              })}
              rounded={false}
              width={300}
              height={400}
              onHardwareBackPress={this.handleBackPress.bind(this)}
              onTouchOutside={() => {
                this.setState({ isVisible: false });
              }}
            >
              <DialogContent>
                <ScrollView style={{ backgroundColor: '#FFF' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>

                    <Text style={{ fontSize: 18, color: 'black', fontFamily: futuraPtMedium }}>ADD TO CART </Text>

                    <Image style={{ marginTop: 10 }} ource={require('../../assets/cross.png')} />
                  </View>
                  <View style={{ marginTop: 5, backgroundColor: 'grey', height: 0.5, width: '100%' }}></View>

                  {
                    (this.state.cartList.length > 0) ?
                      <FlatList
                        vertical
                        data={this.state.cartList}
                        renderItem={item => this.renderCart(item)}>
                      </FlatList>

                      :
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <Text>No Record</Text>
                      </View>

                  }

                  <Text style={{ fontSize: 14, fontFamily: futuraPtBook }}> </Text>


                  <Text style={{ fontSize: 14, fontFamily: futuraPtBook }}>TOTAL</Text>
                  <Text style={{ fontSize: 16, color: 'black', marginTop: 5, fontFamily: futuraPtMedium, fontWeight: '300' }}>RM {this.state.subTotal}</Text>

                  <View style={{ marginTop: 10, marginLeft: 10, marginRight: 10, height: 50 }}>
                    <TouchableOpacity style={{ backgroundColor: '#E4007C', justifyContent: "center", alignItems: 'center', height: 50 }}
                      onPress={() => this.onCheckout()}>
                      <Text style={{ color: 'white', fontSize: 18, fontFamily: futuraPtMedium }}>Checkout</Text>
                    </TouchableOpacity>
                  </View>

                  <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }} onPress={() => {
                    this.setState({ isVisible: false });
                  }}>
                    <Text style={{ fontSize: 18, color: '#E4007C', fontFamily: futuraPtBook }}> Continue Shopping </Text>
                  </TouchableOpacity>

                </ScrollView>
              </DialogContent>
            </Dialog>

            {/******************** Heart Image ***************************** */}

            {/********************* v 2 : Product Details **************************** */}

            <View style={styles.productDetailContainer}>

              <Text style={{ marginLeft: 4, fontSize: 16, color: '#D13D7C', fontFamily: futuraPtBook, }}>{this.state.item.brand}</Text>
              <Text style={{ marginLeft: 4, fontSize: 20, fontWeight: '300', fontFamily: futuraPtMedium, color: '#000', marginTop: 5 }}>{this.state.item.product_name} </Text>
              <Text style={{ marginLeft: 4, fontSize: 16, textDecorationLine: 'line-through', marginTop: 5 }}>{(parseFloat(this.state.item.offer_price)) == 0 ? "" : "RM" + this.state.item.price}</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 4, marginTop: 4 }}>
                <Text style={{ fontSize: 26, color: '#E4007C', fontFamily: futuraPtBook }}>{(parseFloat(this.state.item.offer_price)) == 0 ? "RM" + this.state.item.price : "RM" + this.state.item.offer_price} </Text>
                {this.promotion()}
              </View>

              {/************ Sub view : Rating Bar ****************** */}
              <View style={{ width: 120, flex: 1, flexDirection: 'row', marginBottom: 10, marginTop: 5 }}>
                <StarRating
                  disabled={false}
                  maxStars={5}
                  rating={parseInt(this.state.item.rating)}
                  starSize={20}
                  fullStarColor={'#DAA520'} />
                <Text style={{ marginLeft: 4, fontSize: 14 }}>({this.state.item.rating_count})</Text>
              </View>
            </View>
            <View style={{ backgroundColor: '#EEEEEE', height: 15, marginTop: 20 }}></View>
            {/********************* v3 : Blank View **************************** */}

            <View style={{ flex: 1, height: 500 }}>
              <TabView
                navigationState={this.state}
                renderScene={SceneMap({
                  first: () => <HTMLView html={this.state.item.detail} />,
                  second: () => <HTMLView html={this.state.item.how_to_use} />,
                  third: () => <Reviews product_id={this.state.item.id} />

                })}
                onIndexChange={this._handleIndexChange}
                initialLayout={{ width: Dimensions.get('window').width }}
                useNativeDriver={true}
                renderTabBar={(props) =>

                  <TabBar

                    {...props}
                    labelStyle={{ fontSize: 15, color: '#111111', fontFamily: futuraPtBook, fontWeight: '300' }}

                    style={{ backgroundColor: "#fff", height: 50, }}
                    renderIcon={this.renderIcon}
                    indicatorStyle={{ backgroundColor: "#E4007C" }}
                  >
                  </TabBar>
                }>
              </TabView>


            </View>

            {/********************* Button  **************************** */}

          </View>
        </ScrollView>
        <View style={styles.btnContainer}>
          <TouchableOpacity style={styles.btnTouch}
            onPress={() => this.addCart()} >

            <Image style={{ marginRight: 8 }} source={require('../../assets/icon_add_shopping_cart.png')}></Image>
            <Text style={styles.btnText}>ADD TO CART</Text>

          </TouchableOpacity>
        </View>
      </View>
    );

  }

}
