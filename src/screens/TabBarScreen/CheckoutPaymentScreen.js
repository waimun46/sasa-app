
// import CheckBox from 'react-native-checkbox';
import CheckBox from 'react-native-check-box';
import React, { Component } from 'react';
import { Alert, FlatList, Text, View, Image, ScrollView, TextInput, TouchableOpacity, AsyncStorage, Dimensions } from 'react-native';
import { Container, Header, Content, Body, ListItem, List, Card, Left, Right, Icon } from 'native-base';
import styles from '../../styles/stylePaymentAfterCheckout';
import Dialog, { DialogContent, DialogButton, SlideAnimation } from 'react-native-popup-dialog';

import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
import Config from '../../config/Config'
import { futuraPtMedium, futuraPtBook } from '../../styles/styleText'


const backgroundColor = '#fff'

export default class CheckoutPaymentScreen extends Component {
    static navigationOptions = ({
        title: 'Payment',
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium,
            fontWeight: '500'
        },
    })

    cartList = [];

    constructor(props) {
        super(props);
        this.state = {
            check: true,
            selectedIndex: 0,
            subscribe: false,
            cartList: [],
            user: {},
            location: {},
            itemTotal: 0,
            discountTotal: 0,
            orderTotal: 0,
            evoucher: 0,
            data: {},
            termncon: false,
            modalCC: false,
            paymentMethod: null,
            voucher_code: "",
            post_url: '',
        }
    }

    componentDidMount = async () => {
        this.accesToken = await AsyncStorage.getItem('access_token')
        const cartStorage = await AsyncStorage.getItem('cart')
        this.cartList = JSON.parse(cartStorage);
        var locationStorage = await AsyncStorage.getItem('location')
        // this.getSummary()
        var dataParams = this.props.navigation.getParam('data')

        // var itemTotal = 0
        // var discountTotal = 0
        // for (var i = 0; i < cartList.length; i++) {
        //     itemTotal = itemTotal + parseFloat(cartList[i].price);
        //     discountTotal = discountTotal + parseFloat(cartList[i].member_discount);
        // }

        this.setState({
            user: this.props.navigation.getParam('user'),
            location: JSON.parse(locationStorage),
            cartList: this.cartList,
            itemTotal: dataParams.subtotal,
            discountTotal: dataParams.discount,
            orderTotal: dataParams.total,
            // evoucher: dataParams.evoucher,
        })
    }

    onVoucher() {
        this.props.navigation.navigate('ChooseEvoucher', {
            onSelectVoucher: this.onSelectVoucher.bind(this),
            type: "payment",
            isPayment: true
        })

    }

    onSelectVoucher( data ){
        console.log('onSelectVoucher ', data)
        this.getVoucher(data)
    };

    async getVoucher( voucher ) {
        console.log('voucher ', voucher);
        if( voucher && voucher.id ){
            const result = await this.getSummary( voucher.id )
            console.log('result getVoucher ', result)
            if( result.success && result.data ){
                this.setState({ 
                    itemTotal: result.data.subtotal,
                    discountTotal: result.data.discount,
                    orderTotal: result.data.total
                })
            }
        }
        // await this.setState({ voucher_code: data });

    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "ORDERS":

                // this.props.navigation.navigate('CheckoutPaymentSuccess', { data: json[0] })
                this.props.navigation.navigate('CheckoutWebview', { order: json[0], post_url: json[0].post_url })

                break
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        Alert.alert(e)
    }

    async orders() {

        if (!this.state.termncon) {
            Alert.alert("Please accept Terms and Conditions and Privacy Policy of SASA Malaysia.");
            return;
        }

        if (this.state.paymentMethod == null) {
            Alert.alert("Please Choose Payment Method.");
            return;
        }

        await this.setStateAsync({ isLoading: true });

        var formData = new FormData();

        var cartStorage = await AsyncStorage.getItem('cart')
        let cartList = JSON.parse(cartStorage);

        for (var i = 0; i < cartList.length; i++) {
            formData.append('products[' + i + '][0]', cartList[i].id);
            formData.append('products[' + i + '][1]', cartList[i].amount);
        }

        formData.append('name', this.state.user.name);
        formData.append('phone', this.state.user.phone);
        formData.append('email', this.state.user.email);
        formData.append('email', this.state.user.email);
        formData.append('location_id', this.state.location.id);
        formData.append('subscribe', (this.state.subscribe ? 1 : 0));

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'POST',
            useToken: true,
            data: formData,
            canonicalPath: CanonicalPath.ORDERS
        };

        await Service.request(this.onSuccess, this.onError, "ORDERS", params);

        // this.props.navigation.navigate('CheckoutPaymentSuccess')

    }

    async getSummary( voucherId ){
        console.log('on getSummary')
        let result = { success: false, message: "cant get summary" };
        try {
            const url = Config.baseUrl + CanonicalPath.SUMMARY;
            const data = new FormData();
            data.append( 'secode', Config.secode)
            data.append( 'access_token', this.accesToken)
            if( voucherId ){
                data.append( 'voucher_id', parseInt(voucherId))
            }

            for (var i = 0; i < this.cartList.length; i++) {
                const cart = this.cartList[i];
                data.append('products[' + i + '][0]', cart.id);
                data.append('products[' + i + '][1]', cart.amount);
            }
            const response = await fetch(url, { method: "POST", body: data});
            const res = await response.json();
            console.log('res ', res, response, data)
            const { status, error } = res[0];
            if ( res.length > 0) {
                result = { success: true, message: "get summary success", data: res[0] }
            } else {
                result = { success: false, message: error, data: null }
            }
        } catch (err) {
            console.log('on err ', err)
            result.message = err.message;
        }
        return result;
    }

    renderRow = ({ item, index }) => {

        return (
            <View style={styles.viewText}>
                {/************************************** Item 1 ****************************************** */}
                <View style={styles.styleViewOne} >

                    <View><Card style={{ padding: 4 }}><Image style={{ width: 70, height: 100 }} source={{ uri: item.image_url }}></Image></Card></View>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Text style={{ marginLeft: 4, marginTop: 8, fontSize: 14, fontFamily: futuraPtBook, fontWeight: '300', color: '#000000' }}>{item.brand}</Text>
                        <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 20, fontFamily: futuraPtMedium, fontWeight: '300', color: '#000000' }}>{item.product_name}  </Text>
                        <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, textDecorationLine: 'line-through', fontFamily: futuraPtBook }}>{(parseFloat(item.offer_price)) == 0 ? "" : "RM" + item.price}  </Text>
                        <View style={styles.styleViewOne} >
                            <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, color: '#ff4da6', fontFamily: futuraPtBook }}>{(parseFloat(item.offer_price)) == 0 ? "RM" + item.price : "RM" + item.offer_price}  </Text>
                            <View style={{ flex: 1 }}>
                                <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, color: '#000', textAlign: 'right', marginRight: 10, fontFamily: futuraPtBook }}>Quantity: {item.amount}</Text>
                            </View>
                        </View>
                    </View>


                </View>
                <View style={styles.bottomLine}></View>

            </View>
        )
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: backgroundColor }}>

                <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }}></Progressbar>

                {/****************************** Header ************************************************** */}
                {/* <View>
                    <Text style={styles.textHeader} > {this.state.cartList.length} ITEMS</Text>
                </View> */}

                {
                    (this.state.cartList.length > 0) ?
                        <FlatList
                            vertical
                            data={this.state.cartList}
                            renderItem={item => this.renderRow(item)}>
                        </FlatList>
                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                            <Text>No Record</Text>
                        </View>

                }

                {/********************************* Blank Gray View *********************************************** */}

                <View style={{ height: 20, backgroundColor: '#A9A9A9' }}>

                </View>
                {/*************************************** List 1 ***************************************** */}
                <List>
                    {/*************************************** List Item 1 ***************************************** */}
                    <ListItem>
                        <Left>
                            <TouchableOpacity style={styles.optionView}>
                                {/* <Image style={styles.viewTextTop} source={require('../../assets/icon_customer.png')}></Image> */}
                                <Icon1 name="account-outline" size={30} color="black" />
                                <View style={styles.viewTextTop}>
                                    <Text style={styles.textHeading}>CUSTOMER</Text>
                                </View>
                            </TouchableOpacity>
                        </Left>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Right>
                                <Text style={styles.textHeading}>EDIT</Text>
                            </Right>
                        </TouchableOpacity>

                    </ListItem>
                    {/*************************************** List Item 2 ***************************************** */}


                    <View style={{ marginLeft: 30 }}>
                        <Text style={styles.textHeading}>{this.state.user.name}</Text>
                        <Text style={styles.textHeading}>+60{this.state.user.phone}</Text>
                        <Text style={styles.textHeading}>{this.state.user.email}</Text>
                    </View>




                    {/*************************************** List Item 3 ***************************************** */}
                    <ListItem>
                        <Left>
                            <TouchableOpacity style={styles.optionView}>
                                <Image style={styles.viewTextTop} source={require('../../assets/icon_shopping_bag.png')}></Image>
                                <View style={styles.viewTextTop}>
                                    <Text style={styles.textHeading}>STORE LOCATION</Text>
                                </View>
                            </TouchableOpacity>
                        </Left>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>

                            <Right>
                                <Text style={styles.textHeading}>EDIT</Text>
                            </Right>
                        </TouchableOpacity>
                    </ListItem>
                    {/*************************************** List Item 4 ***************************************** */}
                    <ListItem>
                        <Left>
                            <View style={styles.viewText}>
                                <Text style={styles.textHeading}>{this.state.location.name}</Text>
                                <Text style={styles.textHeading}>{this.state.location.address}</Text>
                            </View>
                        </Left>
                        <Right>

                        </Right>
                    </ListItem>

                </List>

                {/*************************************** Blank Gray View ***************************************** */}

                <View style={{ height: 20, backgroundColor: '#EEEEEE' }}>

                </View>
                {/*************************************** Payment Header ***************************************** */}
                <View>
                    <Text style={styles.textHeader} > PAYMENT METHOD</Text>
                </View>
                {/*************************************** List 2 ***************************************** */}

                <List>

                    {/*************************************** List Item 1 ***************************************** */}
                    <ListItem>
                        <TouchableOpacity style={styles.optionView} onPress={() => this.setState({ modalCC: true })}>

                            <Left>
                                <View style={styles.viewTextTop}>
                                    {
                                        (this.state.paymentMethod != null) ?
                                            <Text style={styles.textHeading}>{this.state.paymentMethod}</Text>

                                            :
                                            <Text style={styles.textHeading}>Credit Card</Text>
                                    }
                                </View>
                            </Left>
                            <Right>
                                <Icon style={{ marginRight: 4 }} name="arrow-forward" />
                            </Right>
                        </TouchableOpacity>

                    </ListItem>

                    {/*************************************** List Item 2 ***************************************** */}

                    <ListItem>
                        <TouchableOpacity style={styles.optionView} onPress={() => this.onVoucher()}>
                            <Left>
                                <Image style={styles.viewTextTop} source={require('../../assets/icon_voucher.png')}></Image>
                                <View style={styles.viewTextTop}>
                                    <Text style={styles.textHeading}>Use E-voucher</Text>
                                </View>
                            </Left>
                            <Right>
                                <Icon style={{ marginRight: 4 }} name="arrow-forward" />
                            </Right>
                        </TouchableOpacity>

                    </ListItem>

                </List>

                {/*************************************** Blank Gray View ***************************************** */}

                <View style={{ height: 20, backgroundColor: '#EEEEEE' }}>

                </View>

                {/*************************************** List 3 ***************************************** */}

                <List>
                    <View style={{ marginTop: 20, marginLeft: 20 }}>

                        <Text style={{
                            color: '#000',
                            fontSize: 20,
                            fontFamily: futuraPtMedium,
                            fontWeight: '300',
                            color: '#111111'
                        }}>ORDER SUMMARY</Text>


                    </View>

                    {/* <View style={{ flex: 1, flexDirection: 'row', marginLeft: 20, marginRight: 20, marginTop: 20, justifyContent: 'space-between' }} >

                        <Text style={styles.textHeading}>Member Point:</Text>


                        <Text style={{ fontSize: 18, fontFamily: futuraPtBook, color: '#000' }}>200 Points</Text>

                    </View> */}

                    <View style={{ flex: 1, flexDirection: 'row', marginLeft: 20, marginRight: 20, marginTop: 15, justifyContent: 'space-between' }} >
                        <Text style={styles.textHeading}>Item Total:</Text>
                        <Text style={styles.textHeading}>RM{this.state.itemTotal}</Text>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', marginLeft: 20, marginRight: 20, marginTop: 15, justifyContent: 'space-between' }} >
                        <Text style={styles.textHeading}>Member Discount:</Text>
                        <Text style={styles.textHeading}>RM{this.state.discountTotal}</Text>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', marginLeft: 20, marginRight: 20, marginTop: 15, justifyContent: 'space-between' }} >
                        <Text style={styles.textHeading}>E-Voucher:</Text>
                        <Text style={styles.textHeading}>{this.state.evoucher}</Text>
                    </View>

                    <View style={{ backgroundColor: '#D3D3D3', height: 0.50, marginLeft: 10, marginRight: 10, marginTop: 10 }}></View>

                    <View style={{ flex: 1, flexDirection: 'row', marginLeft: 20, marginRight: 20, marginBottom: 15, marginTop: 15, justifyContent: 'space-between' }} >
                        <Text style={styles.textHeading1}>Order Total</Text>
                        <Text style={styles.textHeading1}>RM{this.state.orderTotal}</Text>
                    </View>
                </List>

                {/* comment */}

                {/* *************** Check Box 1 **************************** */}
                <View style={styles.bottomLine}></View>

                <ListItem style={styles.Container1}>
                    <CheckBox
                        onClick={() => {
                            this.setState({
                                termncon: !this.state.termncon
                            })
                        }}
                        isChecked={this.state.termncon}
                    />
                    <Body>
                        <Text style={{ marginRight: 10, marginLeft: 10, fontSize: 16, fontFamily: futuraPtBook, fontWeight: '300', color: '#111111' }}>
                            I agree and accept all Terms and Conditions and Privacy Policy of SASA Malaysia.
                        </Text>
                    </Body>
                </ListItem>

                <ListItem style={styles.Container2}>
                    <CheckBox
                        onClick={() => {
                            this.setState({
                                subscribe: !this.state.subscribe
                            })
                        }}
                        isChecked={this.state.subscribe}
                    />
                    <Body>
                        <Text style={{ marginRight: 10, marginLeft: 10, fontSize: 16, fontFamily: futuraPtBook, fontWeight: '300', color: '#111111' }}>
                            I agree to receive newsletter and SMS from SASA Malaysia.
                        </Text>
                    </Body>
                </ListItem>

                {/*************************************** Button ***************************************** */}

                <View >

                    <View style={styles.btnContainer}>
                        <TouchableOpacity style={styles.btnTouch} onPress={() => this.orders()}  >
                            <Text style={styles.btnText}>PROCEED ORDER</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* comment */}

                <ScrollView>
                    <Dialog style={{ marginLeft: 10, marginRight: 10, }}
                        visible={this.state.modalCC}
                        containerStyle={{ justifyContent: 'flex-end' }}
                        dialogAnimation={new SlideAnimation({
                            slideFrom: 'bottom',
                        })}
                        rounded={false}
                        width={Dimensions.get('window').width}
                        height={250}
                        onTouchOutside={() => {
                            this.setState({ modalCC: false });
                        }}
                    >
                        <DialogContent>
                            <View style={styles.container}>
                                <View >
                                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }} onPress={() => {
                                        this.setState({ modalCC: false });
                                    }}>
                                        <Image style={{ height: 30, width: 30, marginTop: 18 }} source={require('../../assets/arrowback.png')} />

                                        <Text style={{ fontSize: 20, fontWeight: '300', color: 'black', marginTop: 20, fontFamily: futuraPtMedium }}>SELECT PAYMENT METHOD </Text>

                                        <Image style={{ height: 25, width: 25, marginTop: 20 }} source={require('../../assets/cross.png')} />
                                    </TouchableOpacity>

                                </View>

                                <View style={{ height: 0.50, color: 'grey', marginTop: 10 }}></View>

                                <View>
                                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                                        onPress={() => {
                                            this.setState({ modalCC: false, ccSelected: 0, paymentMethod: "VISA" })
                                        }}>
                                        {this.state.ccSelected === 0 ? <Text style={styles.textSortSelected}>VISA</Text> : <Text style={styles.textSort}>VISA</Text>}
                                        {this.state.ccSelected === 0 ? <Image style={{ marginTop: 20 }} source={require('../../assets/tick.png')} /> : null}

                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                                        onPress={() => {
                                            this.setState({ modalCC: false, ccSelected: 1, paymentMethod: "Master Card" })
                                        }}>
                                        {this.state.ccSelected === 1 ? <Text style={styles.textSortSelected}>Master Card</Text> : <Text style={styles.textSort}>Master Card</Text>}
                                        {this.state.ccSelected === 1 ? <Image style={{ marginTop: 20 }} source={require('../../assets/tick.png')} /> : null}
                                    </TouchableOpacity>


                                </View>

                            </View>
                        </DialogContent>
                    </Dialog>
                </ScrollView>
            </ScrollView>
        );
    }
}
