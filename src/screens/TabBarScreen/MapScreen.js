import React, { Component } from 'react';
import { Alert, StyleSheet, View, Dimensions, Platform } from 'react-native';
import MapView, { PROVIDER_GOOGLE, } from 'react-native-maps';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';

const OS = Platform.OS;

export default class MapScreen extends Component {
  constructor() {
    super();
    this.state = {
      latitude: 30.72589,
      longitude: 76.75787,
      markers: []
    };
  }

  componentDidMount() {
    this.getCurrentPosition()

  }

  componentWillUnmount() {
    // navigator.geolocation.clearWatch(this.watchID);
  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  onSuccess = async (filter, json) => {
    this.setStateAsync({ isLoading: false });
    switch (filter) {
      case "LOCATIONS":

        var markersArray = []

        for (var i = 0; i < json.length; i++) {
          marker = {
            title: json[i].name,
            coordinates: {
              latitude: parseFloat(json[i].lat),
              longitude: parseFloat(json[i].lng),
            },
          }
          markersArray.push(marker)
        }
        this.setState({
          locations: json,
          markers: markersArray,
        });

        break;
    }
  }

  onError = (filter, e) => {
    this.setStateAsync({ isLoading: false });
    alert(e)
  }

  async locations() {
    await this.setStateAsync({ isLoading: true });

    var data = await {
      "lat": this.state.latitude,
      "lng": this.state.longitude
    }

    var params = await {
      contentType: "multipart/form-data",
      Accept: "application/json",
      keepAlive: true,
      method: 'GET',
      useToken: true,
      data: data,
      canonicalPath: CanonicalPath.LOCATIONS
    };

    await Service.request(this.onSuccess, this.onError, "LOCATIONS", params);
  }

  getCurrentPosition() {
    try {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          console.log("wew delts", position)
          this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          })
          this.locations()

        },
        (error) => {
          switch (error.code) {
            case 1:
              if (Platform.OS === "ios") {
                Alert.alert("", "To find your location, activate the permissions for the application in Settings - Privacy - Location");
              } else {
                Alert.alert("", "To find your location, activate the permissions for the application in Settings - Privacy - Location");
              }
              break;
            default:
              Alert.alert("", "To find your location, activate the permissions for the application in Settings - Privacy - Location");
          }
        }
      );
    } catch (e) {
      alert(e.message || "");
    }
  };

  render() {
    return (
      <MapView style={{ flex: 1 }}
        provider={ OS == 'ios' ? null : PROVIDER_GOOGLE }
        initialRegion={{
          latitude: this.state.latitude,
          longitude: this.state.longitude,
          latitudeDelta: 0.0,
          longitudeDelta: 0.0,
        }}
      >
        {/* <MapView.Marker
          coordinate={this.state.region}
        /> */}

        {
          (this.state.markers.length > 0) ?
            this.state.markers.map(marker => (
              <MapView.Marker
                coordinate={marker.coordinates}
                title={marker.title}
              />
            ))
            :
            null
        }

      </MapView>
      // <View/>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  }
});
