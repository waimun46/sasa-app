import React, { Component } from 'react';
import { FlatList, Alert, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import { Container, Header, Content, Button, ListItem, List, Card } from 'native-base';
import StarRating from 'react-native-star-rating';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
import { futuraPtMedium, futuraPtBook } from '../../styles/styleText'

export default class AddReviewScreen extends Component {

  static navigationOptions = ({
    title: 'Add Review',
    headerLeft: null,
    headerStyle: {
      backgroundColor: 'black',

    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 22,
      fontFamily: futuraPtMedium,
      fontWeight: '500'

    },
  })

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      isVisible1: false,
      selectBrands: false,
      productList: [],
    }

  }

  componentDidMount() {
    this.setState({
      productList: [
        {
          product_id: 1974,
          product_name: " WAVE FOR HER EDP 100ML",
          image_url: "https://sasa.com.my/_uploads/products/10103010102211.jpg",
          rating: 0,
          comment: '',
        }
      ]
    })
  }

  onStarRatingPress(rating, index) {
    const newArray = [...this.state.productList];
    newArray[index].rating = rating;
    this.setState({ productList: newArray });
  }

  onComment(comment, index) {
    const newArray = [...this.state.productList];
    newArray[index].comment = comment;
    this.setState({ productList: newArray });
  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  onSuccess = async (filter, json) => {
    this.setStateAsync({ isLoading: false });

    switch (filter) {
      case "WRITE_REVIEWS" + this.state.productList.length:

        Alert.alert(
          '',
          'Write reviews successfully!',
          [
            { text: "YES", onPress: () => { this.props.navigation.goBack(); } },
          ],
          { cancelable: false }
        )

        break;
    }

  }

  onError = (filter, e) => {
    this.setStateAsync({ isLoading: false });

    Alert.alert(e)
  }

  async write_reviews() {
    await this.setStateAsync({ isLoading: true });

    for (var i = 0; i < this.state.productList.length; i++) {
      var formData = new FormData();

      formData.append('product_id', this.state.productList[i].product_id);
      formData.append('rating', this.state.productList[i].rating);
      formData.append('comment', this.state.productList[i].comment);

      var params = await {
        contentType: "multipart/form-data",
        Accept: "application/json",
        keepAlive: true,
        method: 'POST',
        useToken: true,
        data: formData,
        canonicalPath: CanonicalPath.WRITE_REVIEWS
      };

      await Service.request(this.onSuccess, this.onError, "WRITE_REVIEWS" + (i + 1), params);
    }

  }

  renderRow = ({ item, index }) => {
    return (
      <View style={{backgroundColor: 'white', paddingTop: 20}}>
        <View style={{ flex: 1, flexDirection: 'row',  }} >
          <View><Card style={{ marginLeft: 20 }}><Image style={{ width: 100, height: 100 }} source={{ uri: item.image_url }}></Image></Card></View>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <Text style={{ marginLeft: 20, marginTop: 10, fontSize: 18, fontFamily: futuraPtMedium, fontWeight: '500' }}>{item.product_name}</Text>
            <View style={{ width: 80, height: 60, flex: 1, flexDirection: 'row', marginBottom: 4, marginTop: 8, marginLeft: 20 }}>
              <StarRating
                disabled={false}
                maxStars={5}
                rating={item.rating}
                selectedStar={(rating) => this.onStarRatingPress(rating, index)}
                starSize={30}
                fullStarColor={'#DAA520'} />
            </View>
          </View>
        </View>
        {/********************************* */}
        <View style={{ borderWidth: 1, height: 120, marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 20, borderColor: 'lightgrey' }}>
          <TextInput style={{ marginLeft: 10, fontFamily: futuraPtBook, fontSize: 18 }} multiline={true} placeholder='Write your review...' onChangeText={(comment) => this.onComment(comment, index)} />
        </View>

        <View style={{ marginTop: 4, marginBottom: 4, backgroundColor: 'lightgrey', height: 20, }}></View>
      </View>
    )
  }

  render() {
    return (
      <View style={{ backgroundColor: 'lightgrey', flex: 1 }}>
        <ScrollView>
          <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }}></Progressbar>
          {
            (this.state.productList.length > 0) ?
              <FlatList
                vertical
                data={this.state.productList}
                renderItem={item => this.renderRow(item)}>
              </FlatList>
              :
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'column', marginTop: 30 }}>
                <Text>No Record</Text>
              </View>

          }

          <View style={{ backgroundColor: 'lightgrey' }}>

            <View style={{ marginTop: 10, marginBottom: 10, marginLeft: 20, marginRight: 20, height: 50 }}>
              <TouchableOpacity style={{ backgroundColor: '#E4007C', justifyContent: "center", alignItems: 'center', height: 50 }} onPress={() =>
                this.write_reviews()} >
                <Text style={{ color: 'white', fontSize: 20, fontFamily: futuraPtMedium }}>SUBMIT REVIEW</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/********************************* */}

        </ScrollView>
      </View>




    );
  }
}
