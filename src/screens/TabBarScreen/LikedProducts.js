import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import { Container, Header, Content, Button, ListItem, List, Left, Right } from 'native-base';
import StarRating from 'react-native-star-rating';
import GridView from 'react-native-super-grid';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
import { futuraPtMedium, futuraPtBook } from '../../styles/styleText'

export default class LikedProducts extends Component {
  static navigationOptions = ({
    title: 'Liked Products',
    headerLeft: null,
    headerStyle: {
      backgroundColor: 'black',

    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 22,
      fontFamily: futuraPtMedium,
      fontWeight: '500'
    },
  })
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      isVisible1: false,
      selectBrands: false,
      productList: [],
    }

  }

  componentDidMount(){
    this.productList()
  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  onSuccess = async (filter, json) => {
    this.setStateAsync({ isLoading: false });

    switch (filter) {
      case "LIKED_PRODUCTS":
        const result = json[0];
        if( result.status != 0 ){
            this.setState({
              productList: json
            })
        }
        break;
    }

  }

  onError = (filter, e) => {
    this.setStateAsync({ isLoading: false });

    alert(e)
  }

  async productList() {
    await this.setStateAsync({ isLoading: true });

    var params = await {
      contentType: "multipart/form-data",
      Accept: "application/json",
      keepAlive: true,
      method: 'GET',
      useToken: true,
      data: {},
      canonicalPath: CanonicalPath.LIKED_PRODUCTS
    };

    await Service.request(this.onSuccess, this.onError, "LIKED_PRODUCTS", params);
  }

  promotion(item) {

    if (item.extra_point && item.extra_point != "NONE") {
      return (
        <View style={{ justifyContent: "center", alignItems: 'center', resizeMode: 'cover', backgroundColor: '#F48FB1', marginLeft: 10 }}>
          <Text style={{ color: '#FFFFFF', alignItems: 'center', fontFamily: futuraPtBook }}>
            {item.extra_point} X POINTS
          </Text>
        </View>
      )
    } else if (item.buy_free && item.buy_free != "NONE") {
      return (
        <View style={{ justifyContent: "center", alignItems: 'center', resizeMode: 'cover', backgroundColor: '#F48FB1', marginLeft: 10 }}>
          <Text style={{ color: '#FFFFFF', alignItems: 'center', fontFamily: futuraPtBook }}>
            {item.buy_free}
          </Text>
        </View>
      )
    } else if (item.promotion_type && item.promotion_type != "NONE") {
      return (
        <View style={{ justifyContent: "center", alignItems: 'center', resizeMode: 'cover', backgroundColor: '#F48FB1', marginLeft: 10 }}>
          <Text style={{ color: '#FFFFFF', alignItems: 'center', fontFamily: futuraPtBook }}>
            {item.promotion_type}
          </Text>
        </View>
      )
    } else {
      return null;
    }
  }


  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <ScrollView>
        <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />


          {
            (this.state.productList.length > 0) ?

              <GridView
                itemDimension={130}
                items={this.state.productList}
                style={styles.gridView}
                renderItem={(item, index) => (

                  <View style={{ flexDirection: 'row', backgroundColor: '#fff', marginTop: 20, }}>
                    <View style={{ flexDirection: 'column', flex: 1, }}>
                      <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('ProductDetailsScreen', {
                        item: item,
                      })}
                      >

                        <Image
                          style={{
                            marginLeft: 10,

                            resizeMode: 'cover', height: 200, borderWidth: 1, borderColor: 'grey'
                          }}
                          source={{ uri: item.image_url[index] }}
                        />
                      </TouchableOpacity>
                      {this.promotion(item)}
                      <View style={{ width: 100, height: 20, flex: 1, flexDirection: 'row', marginBottom: 4, marginTop: 8, marginLeft: 10 }}>
                        <StarRating
                          disabled={false}
                          maxStars={5}
                          rating={parseInt(item.rating)}
                          starSize={18}
                          fullStarColor={'#DAA520'} />
                        <Text style={{ marginLeft: 10, fontSize: 14 }}>({item.rating_count})</Text>
                      </View>
                      <Text numberOfLines={2} style={{ marginLeft: 10, fontWeight: '300', fontFamily: futuraPtMedium, fontSize: 18, color: '#000000', marginTop: 5 }}>{item.product_name}</Text>
                      <Text style={{ marginLeft: 10, fontFamily: futuraPtBook, fontSize: 16, fontWeight: '300', marginTop: 5, color: '#111111' }}>by {item.brand}</Text>
                      <Text style={{ textDecorationLine: 'line-through', fontFamily: futuraPtBook, marginLeft: 10, marginTop: 5 }}>{(parseFloat(item.offer_price)) == 0 ? "" : "RM" + item.price}</Text>



                      <List >
                        <ListItem>
                          <Left>
                            <Text style={{ color: '#ff4da6', fontFamily: futuraPtBook, fontWeight: '300', fontSize: 20 }}>{(parseFloat(item.offer_price)) == 0 ? "RM" + item.price : "RM" + item.offer_price}</Text>
                          </Left>
                          <TouchableOpacity>
                            <Right>
                              <View style={{ backgroundColor: '#FFEBEE', height: 40, width: 40, justifyContent: 'center', alignItems: 'center', marginRight: 10 }}>
                                <Image source={require('../../assets/cart.png')} />
                              </View>
                            </Right>
                          </TouchableOpacity>
                        </ListItem>
                      </List>
                    </View>
                  </View>
                )}
              />
              :
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                <Text>No Record</Text>
              </View>

          }


        </ScrollView>

      </View>




    );
  }
}
const styles = StyleSheet.create({
  menuItems: {
    flexDirection: 'row',
    justifyContent: 'center',
    height: 50
  },
  image1: {
    height: 30,
    width: 30,
    marginLeft: 20,
    marginTop: 10
  },
  text1: {
    fontSize: 15,
    marginLeft: 10,
    marginTop: 15
  },
  image2: {
    height: 30,
    width: 30,
    marginLeft: 30,
    marginTop: 10

  },
  text2: {
    fontSize: 15,
    marginTop: 15
  },
  cardContainer: {
    flexDirection: 'row',

    marginTop: 10,
    padding: 10
  }

})
