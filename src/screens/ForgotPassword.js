

import { createStackNavigator, StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import { Col, Row, Grid } from "react-native-easy-grid";
import React, { Component } from 'react';
import styles from '../styles/styleForgotPsw'
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import { Container, Header, Content, Button, ListItem, List } from 'native-base';
import { futuraPtMedium } from '../styles/styleText'
const dimWidth = Dimensions.get('window').width
const dimHeight = Dimensions.get('window').height
const backgroundColor = '#fff'
const buttonColor = '#ff4da6'
const textColor = '#fff'

/**
 *   Code Formating :
 *   1) Select All
 *   2) Shift+Alt+F
 *
 */


export default class ForgotPassword extends Component {
    static navigationOptions = ({
        title: 'Forgot Password',
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily:futuraPtMedium,
            fontWeight:'500'
        },
    })


    constructor(props) {
        super(props);

        this.state = {
            check: true,
            selectedIndex: 0,


            myEmail: '',
            emailError: false
        }
    }


    validation = () => {

        this.setState({
            emailError: false
        })


        if (this.state.myEmail == '') {
            this.setState({
                emailError: true
            })
        }


        else if (this.state.myEmail != '')
        {


            var validator = require("email-validator");

            //validator.validate("test@email.com"); // true
            if(validator.validate(this.state.myEmail))
            {
                //console.log("Email is Correct");
                this.props.navigation.navigate('NewPassword')
            }
            else
            {
                //console.log("Email is Not Correct");
                 this.setState({
                        emailError: true
                    })
            }
        }
    }

    isEmail(value){
        this.setState({
            myEmail: value.replace(/\s/g, '')
        })
     }


    render() {
        return (
            <ScrollView style={styles.containerBackground}>
                <View style={styles.viewCreateNewPsw}>
                    <Text style={styles.textCreateNewPsw} >
                        Reset Password
                    </Text>
                </View>
                <View style={styles.viewResetView}>

                    <Text style={styles.textResetView} >
                        To reset your password, enter your email address below and follow the instructions in the email we'll send you.
                    </Text>
                </View>



                <View style={styles.viewEmail}>
                    <Text style={styles.textEmail} >Email address</Text>
                    <TextInput style={styles.inputTextEmail} placeholder=''
                       onChangeText={myEmail => this.isEmail(myEmail)}
                        value={this.state.myEmail}/>
                </View>
                <View style={styles.downLine}></View>


                {this.state.emailError == true &&
                    <View>
                        <Text style={styles.emailError}> Please enter valid email address</Text>
                    </View>
                }



                <View style={styles.viewButton}>
                    <TouchableOpacity style={styles.touch}
                        onPress={this.validation} >
                        <Text style={styles.textButton}>SEND RESET LINK</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
