import React from 'react';
import {
  DrawerNavigator,
  StackNavigator,
  TabNavigator
} from 'react-navigation';
import Image from 'react-native';
import Home from '../SideMenuScreen/Home';
import Settings from '../SideMenuScreen/';

import Modal from '../SideMenuScreen/Modal/Modal';
import Drawer from '../SideMenuScreen/Drawer';

// Stack navigation for Settings and Profile screens
const SettingsTab = StackNavigator({
  Settings: {
    screen: Settings,
    navigationOptions: {
      header: null,               // Hide the header
      headerBackTitle: 'Back',    // Title back button Back when we navigate to Profile from Settings
    },
  },
  
}, {
  headerMode: 'screen',
});
const TabNavigation = TabNavigator({
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: 'Home',
        tabBarIcon: ({ tintColor, focused }) => <Image
         source={require('../../assets/home.png')}
        />
      },
    },
    Settings: {
      screen: SettingsTab,
      navigationOptions: {
        tabBarLabel: 'Settings',
        tabBarIcon: ({ tintColor, focused }) => <Image
        source={require('../../assets/home.png')}
        />
      },
    },
  });
  
  // Wrap tab navigation into drawer navigation
  const TabsWithDrawerNavigation = DrawerNavigator({
    Tabs: {
      screen: TabNavigation,
    }
  }, {
    // Register custom drawer component
    contentComponent: props => <Drawer {...props} />
  });
  
  // And lastly stack together drawer with tabs and modal navigation
  // because we want to be able to call Modal screen from any other screen
  export default StackNavigator({
    TabsWithDrawer: {
      screen: TabsWithDrawerNavigation,
    },
    Modal: {
      screen: Modal
    },
  }, {
    // In modal mode screen slides up from the bottom
    mode: 'modal',
    // No headers for modals. Otherwise we'd have two headers on the screen, one for stack, one for modal.
    headerMode: 'none',
  });