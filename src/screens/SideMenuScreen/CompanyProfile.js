import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';

import HTML from 'react-native-render-html';

export default class CompanyProfile extends Component {

    static navigationOptions = ({
        title: 'Company Profile ',
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff',

    })

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            item: {},
        }

    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "ABOUTS":
                this.setState({
                    item: json[0]
                })

                break
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        alert(e)
    }

    async abouts() {
        await this.setStateAsync({ isLoading: true });

        var data = await {
            "type": "company_profile"
        }

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: data,
            canonicalPath: CanonicalPath.ABOUTS
        };

        await Service.request(this.onSuccess, this.onError, "ABOUTS", params);
    }

    componentDidMount() {
        this.abouts()
    }

    render() {
        return (
            <ScrollView style={{ margin: 20, }}  >

                <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />

                <HTML html={this.state.item.description} />

            </ScrollView>
        );
    }
}