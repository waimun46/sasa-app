
import React, { Component } from 'react';
import { Platform, FlatList, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions, Alert } from 'react-native';
import { Container, Header, Content, Button, ListItem, List, Left, Right } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from '../styles/styleOnlinePurchaseTransaction'

import Moment from 'moment';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../config/Service';
import CanonicalPath from '../config/CanonicalPath';
import { futuraPtMedium } from '../styles/styleText'

export default class MyTransaction extends Component {
    static navigationOptions = ({
        title: '',
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black',

        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium,
            fontWeight: '500'
        },
    })
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            transactionsOnline: [],
        }

    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "TRANSACTION_ONLINE":

                this.setState({
                    transactionsOnline: json,
                })

                break
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        alert(e)
    }

    async transactions_online() {
        await this.setStateAsync({ isLoading: true });

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: {},
            canonicalPath: CanonicalPath.TRANSACTION_ONLINE
        };

        await Service.request(this.onSuccess, this.onError, "TRANSACTION_ONLINE", params);
    }

    renderRow = (item) => {
        var dateString = item.item.date;
        var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
        var date = momentObj.format('DD MMMM YYYY');
        return (
            <ListItem selected>
                <Left>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('BillNoScreen', {item: item.item})} >

                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={styles.containerLeft} >
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontWeight: '300', fontSize: 18, color: '#000000', fontFamily: futuraPtMedium }}>Order No: #{item.item.order_id}</Text>

                                    <Icon name="angle-right" size={25} color="lightgrey" style={{ marginLeft: 190, marginRight: 20 }} />

                                </View>
                                <Text style={styles.textHeading}>Date: {date}</Text>
                                <Text style={styles.textHeading1}>Total: RM{item.item.total}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                </Left>
                <Right>
                    <View style={styles.containerRight} >
                        <TouchableOpacity style={styles.signinTouch} onPress={this.validUser} >
                            <Text style={{ fontWeight: '300', color: '#111111', fontFamily: futuraPtMedium, fontSize: 14 }}>{item.item.status}</Text>
                        </TouchableOpacity>
                    </View>

                </Right>
            </ListItem>
        )
    }

    componentDidMount() {
        this.transactions_online()
    }

    render() {
        return (

            <View style={{ flex: 1, backgroundColor: '#fff' }}>

                <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />


                {
                    (this.state.transactionsOnline.length > 0) ?
                        <FlatList
                            vertical
                            data={this.state.transactionsOnline}
                            renderItem={item => this.renderRow(item)}>
                        </FlatList>
                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                            <Text>No Record</Text>
                        </View>

                }

            </View>
        )
    }
}
