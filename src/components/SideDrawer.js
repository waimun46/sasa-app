import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Image,ScrollView,TextInput,TouchableOpacity,ImageBackground,Dimensions} from 'react-native';
export const SideDrawer = props => {
    let { activeTintColor, activeBackgroundColor } = props;
  
    return ( //your implementation
      <View
        style={{
          backgroundColor: activeBackgroundColor
        }}
      >
        <Text style={{ color: activeTintColor }}>Title</Text>
      </View>
    );
  };