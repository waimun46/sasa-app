import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import React, { Component } from 'react';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';

export default class Slider extends Component {

    render() {
        return (
            <View style={{ flex: 1, }}>
                <IndicatorViewPager
                    style={{ height: 180 }}
                    indicator={this._renderDotIndicator()}
                >
                    <View style={{ backgroundColor: 'rgb(223, 225, 223)', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => this.props.click.navigation.navigate('ItemsPictures')}>
                            <Image style={{ height: 180, width: 180 }} source={require('../../src/assets/pic1.png')}></Image>
                        </TouchableOpacity>
                    </View>
                    <View style={{ backgroundColor: 'rgb(223, 225, 223)', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => this.props.click.navigation.navigate('ItemsPictures')}>
                            <Image style={{ height: 180, width: 180 }} source={require('../../src/assets/pic1.png')}></Image>
                        </TouchableOpacity>
                    </View>
                    <View style={{ backgroundColor: 'rgb(223, 225, 223)', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => this.props.click.navigation.navigate('ItemsPictures')}>
                            <Image style={{ height: 180, width: 180 }} source={require('../../src/assets/pic1.png')}></Image>
                        </TouchableOpacity>
                    </View>
                    
                </IndicatorViewPager>

            </View>
        );
    }

    _renderTitleIndicator() {
        return <PagerTitleIndicator titles={['one', 'two', 'three']} />;
    }

    _renderDotIndicator() {
        return <PagerDotIndicator pageCount={3} />;
    }



}