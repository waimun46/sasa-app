import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import SplashScreen from './src/screens/SplashScreen';
import AuthStack from './src/AuthStack'
import Drawer from './src/DrawerStack'

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: SplashScreen,
    App: Drawer,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));